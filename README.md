# nuts-fast

#### 项目说明
- nuts-fast是一个基于springboot、mybatis-plus的开箱即用的后台管理系统
- nuts-fast是一个属于个人学习交流性质的项目，目前已经实现后台管理基本功能，未来计划更新基于activiti7.8的工作流功能、登录及操作日志功能等，欢迎交流学习。
- 本项目目前处于早期测试版本，尚有较多的bug，例如未进行后端必输校验、格式化JSR303报错格式、日志脱敏等，同时本项目目前由我一人在工作之余开发，若有bug可以及时提出，有新需求想让我添加的也可以提出issue，大家一起交流学习，共同进步，争取早日实现企业级中后台管理框架的研发！
- 前端地址：https://gitee.com/Qiex/nuts-ui

#### 内置功能
1.  用户管理: 用户是系统的主要操作人员，该功能用于配置登录用户的各项信息。
2.  菜单管理: 配置整个系统的各项功能的菜单、操作等。
3.  角色管理: 本项目作为RABC权限模型为基准开发的后台权限系统，角色功能主要作为用户的前端登录控制，后期计划添加颗粒度至api级。
4.  字典管理: 整个项目使用统一的字典管理模块进行控制，前端也设计了专门的字典组件。
5.  系统参数: 对系统动态配置常用参数。
6.  系统公告: 系统通知公告信息发布维护，目前已经实现了通知功能。
7.  。。。

### 特色功能
1.  日志脱敏: 可以通过注解的方式进行日志脱敏，保证敏感数据不泄露。
2.  响应脱敏: 可以通过注解的方式进行响应脱敏，保证敏感数据不泄露。
3.  文件管理: 系统使用 Minio分布式文件系统作为文件管理系统，可视化操作管理文件。
4.  。。。

#### 未来计划
1. 流程模型在线设计
2. 流程实例管理
3. 个人流程管理(待办、已办、申请、关注)
4. 登录日志在线查看
5. 请求日志在线查看
6. 登录状态管理
7. 在线表单设计
8. 代码生成
9. 。。。

#### 项目特点
- 使用jwt token作为项目的登录状态管理，基于spring security完成项目级的权限认证管理
- 完全基于Mybatis-Plus的新特性进行项目开发，便于二次开发与代码理解
- 使用h2数据库以及嵌入式redis作为测试、开发环境数据库以及缓存组件，便于展示及学习
- 较为灵活的权限控制，可以控制到菜单及按钮，满足大多数情况
- 使用open api3引入swagger文档，便于编写api文档

#### 技术选型
- Spring Boot 2.7.6
- Spring Security 5.7.5
- redisson 3.18.0
- Mybatis Plus 3.5.2
- Activiti 7.8.0

#### 部署方式
- 直接启动即可，使用内嵌的h2数据库及redis，启动时自动写入sql

#### 演示地址
- 演示地址将在近期放出，尽情期待

#### 联系方式
- 邮箱 yaya_tech@aliyun.com 欢迎大家提出问题、进行交流

#### 系统截图
<table>
    <tr>
      <td><img src="./src/main/resources/public/login.jpg" /></td>
      <td><img src="./src/main/resources/public/home.jpg" /></td>
    </tr>
    <tr>
      <td><img src="./src/main/resources/public/menu2.jpg" /></td>
      <td><img src="./src/main/resources/public/menu.jpg" /></td>
    </tr>
    <tr>
      <td><img src="./src/main/resources/public/user.jpg" /></td>
      <td><img src="./src/main/resources/public/notice.jpg" /></td>
    </tr>
    <tr>
      <td><img src="./src/main/resources/public/dict.jpg" /></td>
      <td><img src="./src/main/resources/public/search.jpg" /></td>
    </tr>
    <tr>
      <td><img src="./src/main/resources/public/dark.jpg" /></td>
      <td><img src="./src/main/resources/public/dark2.jpg" /></td>
    </tr>
</table>