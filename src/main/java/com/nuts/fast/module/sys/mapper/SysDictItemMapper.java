package com.nuts.fast.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuts.fast.module.sys.model.entity.SysDictItemDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统管理 - 字典项信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItemDO> {
}
