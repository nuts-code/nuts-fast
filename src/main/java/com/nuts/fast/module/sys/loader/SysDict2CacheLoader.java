package com.nuts.fast.module.sys.loader;

import com.nuts.fast.module.sys.service.intf.ISysDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 系统字典启动时加载至缓存
 *
 * @author Nuts
 */
@Slf4j
@Component
@Order(1)
public class SysDict2CacheLoader implements CommandLineRunner {

    @Resource
    private ISysDictService sysDictService;

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) {

        sysDictService.getDictWithDictItemList();
        log.debug("==================系统字典已加载==================");
    }
}
