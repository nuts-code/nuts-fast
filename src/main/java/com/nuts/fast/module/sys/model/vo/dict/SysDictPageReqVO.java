package com.nuts.fast.module.sys.model.vo.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 系统管理 - 字典分页查询请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictPageReqVO", description = "系统管理 - 字典分页查询请求 VO")
public class SysDictPageReqVO implements Serializable {

    /**
     * 字典名称
     */
    @Schema(name = "dictName", description = "字典名称")
    private String dictName;

    /**
     * 字典描述
     */
    @Schema(name = "dictDesc", description = "字典描述")
    private String dictDesc;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}
