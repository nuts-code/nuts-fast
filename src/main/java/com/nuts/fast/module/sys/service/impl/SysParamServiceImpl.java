package com.nuts.fast.module.sys.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.redis.constant.RedisConstants;
import com.nuts.fast.module.sys.model.convert.SysParamConvert;
import com.nuts.fast.module.sys.model.entity.SysParamDO;
import com.nuts.fast.module.sys.model.vo.param.SysParamSaveReqVO;
import com.nuts.fast.module.sys.mapper.SysParamMapper;
import com.nuts.fast.module.sys.model.vo.param.SysParamInfoResVO;
import com.nuts.fast.module.sys.model.vo.param.SysParamPageReqVO;
import com.nuts.fast.module.sys.model.vo.param.SysParamPageResVO;
import com.nuts.fast.module.sys.service.intf.ISysParamService;
import lombok.val;
import org.redisson.api.RBucket;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 系统管理 - 系统参数信息服务实现类
 *
 * @author Nuts
 */
@Service
public class SysParamServiceImpl extends ServiceImpl<SysParamMapper, SysParamDO> implements ISysParamService {

    /**
     * 默认参数缓存时间 24h
     */
    private static final long TIME_STEP = 60 * 60 * 24L;

    @Resource
    private SysParamMapper sysParamMapper;

    @Resource
    private RedissonClient redissonClient;

    /**
     * 获取系统参数列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 系统参数列表分页信息
     */
    @Override
    public IPage<SysParamPageResVO> page(PageModel<SysParamPageReqVO> pageParams) {
        return sysParamMapper.getSysParamListByPage(new Page<>(
                        pageParams.getCurrent(),
                        pageParams.getSize()),
                pageParams.getParams());
    }

    /**
     * 通过系统参数主键获取系统参数详情信息
     *
     * @param id 系统参数主键
     * @return 系统参数详情信息
     */
    @Override
    public SysParamInfoResVO getInfoById(Long id) {
        return sysParamMapper.getSysParamInfoById(id);
    }

    /**
     * 从数据库中加载参数缓存
     *
     * @return 参数缓存
     */
    @Override
    public Map<String, String> updateParamMapCache() {

        RMapCache<String, Map<String, String>> cache = redissonClient.getMapCache(RedisConstants.PARAM_MAP);
        RBucket<String> rBucket = redissonClient.getBucket(RedisConstants.PARAM_MD5);

        Map<String, String> paramMapBack = new HashMap<>(8);
        Map<String, String> paramMapFront = new HashMap<>(8);

        sysParamMapper.getSysParamList().forEach(sysParamCache -> {
            if ("F".equals(sysParamCache.getParamType())) {
                paramMapFront.put(sysParamCache.getParamKey(), sysParamCache.getParamValue());
            }
            paramMapBack.put(sysParamCache.getParamKey(), sysParamCache.getParamValue());
        });

        // 将参数缓存进 Redisson
        cache.put("param_map_back", paramMapBack, TIME_STEP, TimeUnit.SECONDS);
        cache.put("param_map_front", paramMapFront, TIME_STEP, TimeUnit.SECONDS);
        // 将参数 MD5 的值存入缓存，用于版本校验
        rBucket.set(SecureUtil.md5(paramMapFront.toString()), TIME_STEP, TimeUnit.SECONDS);

        return paramMapFront;
    }

    /**
     * 获取所有生效的参数
     *
     * @return 参数列表
     */
    @Override
    public Map<String, String> getParamMap() {

        RMapCache<String, Map<String, String>> cacheFront = redissonClient.getMapCache(RedisConstants.PARAM_MAP);

        Map<String, String> cacheParamMapFront = cacheFront.get("param_map_front");
        Map<String, String> cacheParamMapBack = cacheFront.get("param_map_back");

        if (cacheParamMapFront == null || cacheParamMapFront.isEmpty()
                || cacheParamMapBack == null || cacheParamMapBack.isEmpty()) {
            log.debug("加载前端参数 - 数据库");
            cacheParamMapFront = updateParamMapCache();
        } else {
            log.debug("加载前端参数 - redis");
        }

        return cacheParamMapFront;
    }

    /**
     * 保存系统参数信息
     *
     * @param sysParamSaveReq 系统参数保存请求信息
     * @return 系统参数主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveOrUpdate(SysParamSaveReqVO sysParamSaveReq) {
        Long id = sysParamSaveReq.getId();

        if (ObjectUtil.isEmpty(id)) {
            id = insertSysParam(sysParamSaveReq);
        } else {
            updateSysParam(sysParamSaveReq);
        }
        return id;
    }

    /**
     * 根据系统参数主键删除系统参数信息
     *
     * @param id 系统参数主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    /**
     * 根据系统参数主键列表批量删除系统参数信息
     *
     * @param ids 系统参数主键列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeByIds(Collection<?> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 新增系统参数信息
     *
     * @param sysParamSaveReq 系统参数保存请求信息
     * @return 系统参数主键
     */
    private Long insertSysParam(SysParamSaveReqVO sysParamSaveReq) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val param = SysParamConvert.INSTANCE
                .saveReqVoConvert2Do(sysParamSaveReq)
                .withCreatedBy(currentUserId)
                .withCreatedTime(new Date())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        sysParamMapper.insert(param);

        return param.getId();
    }

    /**
     * 修改系统参数信息
     *
     * @param sysParamSaveReq 系统参数保存请求信息
     */
    private void updateSysParam(SysParamSaveReqVO sysParamSaveReq) {

        val oldParam = sysParamMapper.selectById(sysParamSaveReq.getId());

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val param = SysParamConvert.INSTANCE
                .saveReqVoConvert2Do(oldParam, sysParamSaveReq)
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        sysParamMapper.updateById(param);
    }
}
