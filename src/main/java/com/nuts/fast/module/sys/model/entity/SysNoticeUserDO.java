package com.nuts.fast.module.sys.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 系统通知用户记录表
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "系统管理 - 系统通知用户记录表", description = "系统管理 - 系统通知用户记录表")
@TableName("sys_notice_user")
public class SysNoticeUserDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 通知主键
     */
    @Schema(name = "nid", description = "通知主键")
    private Long nid;

    /**
     * 用户主键
     */
    @Schema(name = "uid", description = "用户主键")
    private Long uid;

    /**
     * 状态
     * q - 已读, 0 - 未读
     */
    @Schema(name = "status", description = "状态;1 - 已读, 0 - 未读")
    private Integer status;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 已读时间
     */
    @Schema(name = "readTime", description = "已读时间")
    private Date readTime;
}
