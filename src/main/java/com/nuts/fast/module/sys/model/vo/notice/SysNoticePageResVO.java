package com.nuts.fast.module.sys.model.vo.notice;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 通知公告信息分页查询响应 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysNoticePageResVO", description = "系统管理 - 通知公告信息分页查询响应 VO")
public class SysNoticePageResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 通知标题
     */
    @Schema(name = "noticeTitle", description = "通知标题")
    private String noticeTitle;

    /**
     * 通知推送人
     */
    @Schema(name = "noticeByName", description = "通知推送人")
    private String noticeByName;

    /**
     * 通知推送时间
     */
    @Schema(name = "noticeTime", description = "通知推送时间")
    private Date noticeTime;

    /**
     * 是否定时自动推送
     * Y - 是 N - 否
     */
    @Schema(name = "autoNotice", description = "是否定时自动推送；Y - 是 N - 否")
    private String autoNotice;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "createdByName", description = "创建人")
    private String createdByName;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedByName", description = "更新人")
    private String updatedByName;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}
