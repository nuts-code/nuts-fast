package com.nuts.fast.module.sys.model.vo.notice;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 通知公告信息分页查询请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysNoticePageReqVO", description = "系统管理 - 通知公告信息分页查询请求 VO")
public class SysNoticePageReqVO implements Serializable {

    /**
     * 通知标题
     */
    @Schema(name = "noticeTitle", description = "通知标题")
    private String noticeTitle;

    /**
     * 通知推送时间查询起始时间
     */
    @Schema(name = "noticeTimeStartWith", description = "通知推送时间查询起始时间")
    private Date noticeTimeStartWith;

    /**
     * 通知推送时间查询结束时间
     */
    @Schema(name = "noticeTimeEndWith", description = "通知推送时间查询结束时间")
    private Date noticeTimeEndWith;

    /**
     * 是否定时自动推送
     * Y - 是 N - 否
     */
    @Schema(name = "autoNotice", description = "是否定时自动推送；Y - 是 N - 否")
    private String autoNotice;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}
