package com.nuts.fast.module.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.module.sys.mapper.SysNoticeUserMapper;
import com.nuts.fast.module.sys.model.entity.SysNoticeUserDO;
import com.nuts.fast.module.sys.service.intf.ISysNoticeUserService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 系统管理 - 通知用户关联信息服务实现类
 *
 * @author Nuts
 */
@Service
public class SysNoticeUserServiceImpl extends ServiceImpl<SysNoticeUserMapper, SysNoticeUserDO> implements ISysNoticeUserService {

    /**
     * 通过用户主键获取当前用户的通知列表
     *
     * @param uid 用户主键
     * @return 当前用户的通知列表
     */
    @Override
    public List<SysNoticeUserDO> getSysNoticeUserListByUid(Long uid) {
        return this.lambdaQuery()
                .eq(SysNoticeUserDO::getUid, uid)
                .orderBy(true, false, SysNoticeUserDO::getCreatedTime)
                .list();
    }

    /**
     * 根据通知主键删除通知用户关联信息
     *
     * @param nid 通知主键
     * @return 成功标志
     */
    @Override
    public boolean removeByNid(Serializable nid) {
        return this.lambdaUpdate().eq(SysNoticeUserDO::getNid, nid).remove();
    }

    /**
     * 根据通知主键列表批量删除通知用户关联信息
     *
     * @param nids 通知主键列表
     * @return 成功标志
     */
    @Override
    public boolean removeByNids(Collection<?> nids) {
        return this.lambdaUpdate().in(SysNoticeUserDO::getNid, nids).remove();
    }
}
