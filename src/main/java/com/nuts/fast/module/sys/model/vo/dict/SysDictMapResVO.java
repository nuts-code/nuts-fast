package com.nuts.fast.module.sys.model.vo.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictMapResVO", description = "系统管理 - 字典详情信息返回 VO")
public class SysDictMapResVO implements Serializable {

    /**
     * 字典名称
     */
    @Schema(name = "dictMap", description = "字典名称")
    private Map<String, List<SysDictItemResVO>> dictMap;
}
