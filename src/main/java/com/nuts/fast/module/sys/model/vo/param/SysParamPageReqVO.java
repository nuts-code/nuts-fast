package com.nuts.fast.module.sys.model.vo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 系统管理 - 系统参数分页查询请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysParamPageReqVO", description = "系统管理 - 系统参数分页查询请求 VO")
public class SysParamPageReqVO implements Serializable {

    /**
     * 参数名称
     */
    @Schema(name = "paramName", description = "参数名称")
    private String paramName;

    /**
     * 参数类型
     */
    @Schema(name = "paramType", description = "参数类型")
    private String paramType;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}
