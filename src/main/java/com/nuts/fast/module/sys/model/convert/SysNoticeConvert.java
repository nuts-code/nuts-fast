package com.nuts.fast.module.sys.model.convert;

import com.nuts.fast.module.sys.model.entity.SysNoticeDO;
import com.nuts.fast.module.sys.model.vo.notice.SysNoticeSaveReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * 系统通知信息 POJO 转换工具
 *
 * @author Nuts
 */
@Mapper(componentModel = "spring")
public interface SysNoticeConvert {

    SysNoticeConvert INSTANCE = Mappers.getMapper(SysNoticeConvert.class);

    /**
     * 系统通知前端保存请求VO转换系统通知信息实体类工具
     *
     * @param sysNoticeSaveReqVO 系统通知前端保存请求VO
     * @return 系统通知信息实体类
     */
    SysNoticeDO saveReqVoConvert2Do(SysNoticeSaveReqVO sysNoticeSaveReqVO);

    /**
     * 系统通知前端保存请求VO转换系统通知信息实体类工具(修改使用，仅包含部分字段)
     *
     * @param sysNoticeDO        系统通知信息实体类
     * @param sysNoticeSaveReqVO 系统通知前端保存请求VO
     * @return 系统通知信息实体类
     */
    @Mappings({
            @Mapping(source = "sysNoticeSaveReqVO.id", target = "id"),
            @Mapping(source = "sysNoticeSaveReqVO.noticeTitle", target = "noticeTitle"),
            @Mapping(source = "sysNoticeSaveReqVO.noticeContent", target = "noticeContent"),
            @Mapping(source = "sysNoticeSaveReqVO.noticeTime", target = "noticeTime"),
            @Mapping(source = "sysNoticeSaveReqVO.autoNotice", target = "autoNotice"),
            @Mapping(source = "sysNoticeSaveReqVO.status", target = "status"),
            @Mapping(source = "sysNoticeSaveReqVO.remark", target = "remark")
    })
    SysNoticeDO saveReqVoConvert2Do(SysNoticeDO sysNoticeDO, SysNoticeSaveReqVO sysNoticeSaveReqVO);
}
