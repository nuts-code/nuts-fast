package com.nuts.fast.module.sys.model.vo.notice;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.Date;

/**
 * 当前用户通知 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "CurrentUserNoticeVO", description = "当前用户通知 VO")
public class CurrentUserNoticeVO {

    /**
     * 通知主键
     */
    @Schema(name = "id", description = "通知主键")
    private Long id;

    /**
     * 通知标题
     */
    @Schema(name = "noticeTitle", description = "通知标题")
    private String noticeTitle;

    /**
     * 通知人
     */
    @Schema(name = "noticeByName", description = "通知人")
    private String noticeByName;

    /**
     * 推送时间
     */
    @Schema(name = "noticeTime", description = "推送时间")
    private Date noticeTime;

    /**
     * 状态
     * q - 已读, 0 - 未读
     */
    @Schema(name = "status", description = "状态;1 - 已读, 0 - 未读")
    private Integer status;
}
