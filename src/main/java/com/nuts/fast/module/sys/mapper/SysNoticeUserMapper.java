package com.nuts.fast.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuts.fast.module.sys.model.entity.SysNoticeUserDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统管理 - 通知与用户关联信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface SysNoticeUserMapper extends BaseMapper<SysNoticeUserDO> {

}
