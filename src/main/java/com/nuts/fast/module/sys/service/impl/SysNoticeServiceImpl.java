package com.nuts.fast.module.sys.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.auth.service.intf.IAuthUserService;
import com.nuts.fast.module.sys.mapper.SysNoticeMapper;
import com.nuts.fast.module.sys.model.convert.SysNoticeConvert;
import com.nuts.fast.module.sys.model.entity.SysNoticeDO;
import com.nuts.fast.module.sys.model.entity.SysNoticeUserDO;
import com.nuts.fast.module.sys.model.vo.notice.*;
import com.nuts.fast.module.sys.service.intf.ISysNoticeService;
import com.nuts.fast.module.sys.service.intf.ISysNoticeUserService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 系统管理 - 通知公告信息服务实现类
 *
 * @author Nuts
 */
@Slf4j
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNoticeDO> implements ISysNoticeService {

    @Resource
    private ISysNoticeUserService sysNoticeUserService;

    @Resource
    private IAuthUserService authUserService;

    /**
     * 获取通知公告列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 通知公告列表分页信息
     */
    @Override
    public IPage<SysNoticePageResVO> page(PageModel<SysNoticePageReqVO> pageParams) {
        return baseMapper.getSysNoticeListByPage(new Page<>(pageParams.getCurrent(), pageParams.getSize()), pageParams.getParams());
    }

    /**
     * 通过通知公告主键获取通知公告详情信息
     *
     * @param id 通知公告主键
     * @return 通知公告详情信息
     */
    @Override
    public SysNoticeInfoResVO getInfoById(Long id) {
        return baseMapper.getSysNoticeInfoById(id);
    }

    /**
     * 保存通知公告信息
     *
     * @param sysNoticeSaveReq 通知公告保存请求信息
     * @return 通知公告主键
     */
    @Override
    public Long saveOrUpdate(SysNoticeSaveReqVO sysNoticeSaveReq) {

        Long id = sysNoticeSaveReq.getId();

        if (ObjectUtil.isEmpty(id)) {
            id = insertSysNotice(sysNoticeSaveReq);
        } else {
            updateSysNotice(sysNoticeSaveReq);
        }

        return id;
    }

    /**
     * 根据主键删除通知信息
     *
     * @param id 主键ID
     * @return 成功标志
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        // 删除通知信息, 同时删除通知用户关联关系表
        return super.removeById(id) && sysNoticeUserService.removeByNid(id);
    }

    /**
     * 根据主键列表批量删除通知信息
     *
     * @param ids 主键列表
     * @return 成功标志
     */
    @Override
    public boolean removeByIds(Collection<?> ids) {
        // 删除通知信息, 同时删除通知用户关联关系表
        return super.removeByIds(ids) && sysNoticeUserService.removeByIds(ids);
    }

    private Long insertSysNotice(SysNoticeSaveReqVO sysNoticeSaveReq) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val notice = SysNoticeConvert.INSTANCE
                .saveReqVoConvert2Do(sysNoticeSaveReq)
                .withCreatedBy(currentUserId)
                .withCreatedTime(new Date())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        this.save(notice);

        return notice.getId();
    }

    private void updateSysNotice(SysNoticeSaveReqVO sysNoticeSaveReq) {

        val oldNotice = this.getById(sysNoticeSaveReq.getId());

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();


        val notice = oldNotice
                .withNoticeTitle(sysNoticeSaveReq.getNoticeTitle())
                .withNoticeContent(sysNoticeSaveReq.getNoticeContent())
                .withNoticeTime(sysNoticeSaveReq.getNoticeTime())
                .withAutoNotice(sysNoticeSaveReq.getAutoNotice())
                .withStatus(sysNoticeSaveReq.getStatus())
                .withRemark(sysNoticeSaveReq.getRemark())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        this.updateById(notice);
    }

    /**
     * 自动推送公告(每分钟查询是否有新生效的公告)
     */
    @Scheduled(cron = "0 0/1 * * * *")
    @Transactional(rollbackFor = Exception.class)
    public void noticeScheduled() {

        this.lambdaUpdate()
                .eq(SysNoticeDO::getStatus, 1)
                .eq(SysNoticeDO::getAutoNotice, "Y")
                .le(SysNoticeDO::getNoticeTime, new Date())
                .set(SysNoticeDO::getStatus, 2)
                .update();

        log.debug("================= 更新公告推送状态 ==================");

        // 待推送公告列表
        val ready2NoticeIds = this.lambdaQuery().eq(SysNoticeDO::getStatus, 2).list().stream()
                .map(SysNoticeDO::getId)
                .toList();

        if (ObjectUtil.isNotEmpty(ready2NoticeIds)) {
            authUserService.list().forEach(user ->
                    ready2NoticeIds.forEach(id ->
                            sysNoticeUserService.save(SysNoticeUserDO.builder()
                                    .nid(id)
                                    .uid(user.getId())
                                    .status(0)
                                    .createdTime(new Date())
                                    .build())));

            this.lambdaUpdate()
                    .in(SysNoticeDO::getId, ready2NoticeIds)
                    .set(SysNoticeDO::getStatus, 3)
                    .update();

            log.debug("================= 新增推送公告 {} 条 ==================", ready2NoticeIds.size());
        }
    }

    /**
     * 手动推送公告
     *
     * @param ids 要推送的通知公告主键列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendNotice(Collection<?> ids) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        this.lambdaUpdate()
                .in(SysNoticeDO::getId, ids)
                .set(SysNoticeDO::getNoticeBy, currentUserId)
                .set(SysNoticeDO::getStatus, 2)
                .update();
    }

    /**
     * 根据用户主键获取公告列表
     *
     * @return 公告列表
     */
    @Override
    public List<CurrentUserNoticeVO> getCurrentUserNotices() {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        return this.baseMapper.getNoticeListByUid(currentUserId);
    }

    /**
     * 根据通知公告主键已读通知公告信息
     *
     * @param id 通知公告主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void readNotice(Long id) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        this.sysNoticeUserService.lambdaUpdate()
                .eq(SysNoticeUserDO::getUid, currentUserId)
                .eq(SysNoticeUserDO::getNid, id)
                .eq(SysNoticeUserDO::getStatus, 0)
                .set(SysNoticeUserDO::getReadTime, new Date())
                .set(SysNoticeUserDO::getStatus, 1)
                .update();
    }

    /**
     * 全部已读
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void readAllNotice() {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        this.sysNoticeUserService.lambdaUpdate()
                .eq(SysNoticeUserDO::getUid, currentUserId)
                .eq(SysNoticeUserDO::getStatus, 0)
                .set(SysNoticeUserDO::getReadTime, new Date())
                .set(SysNoticeUserDO::getStatus, 1)
                .update();
    }
}
