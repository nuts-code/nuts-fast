package com.nuts.fast.module.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.module.sys.mapper.SysDictItemMapper;
import com.nuts.fast.module.sys.model.entity.SysDictItemDO;
import com.nuts.fast.module.sys.service.intf.ISysDictItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;

/**
 * 系统管理 - 字典项信息服务实现类
 *
 * @author Nuts
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItemDO> implements ISysDictItemService {

    /**
     * 根据字典主键删除字典项信息
     *
     * @param did 字典主键
     * @return 成功标志
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeByDid(Serializable did) {
        return this.lambdaUpdate().eq(SysDictItemDO::getDid, did).remove();
    }

    /**
     * 根据字典主键列表删除字典项信息
     *
     * @param dids 字典主键列表
     * @return 成功标志
     */
    @Override
    public boolean removeByDids(Collection<?> dids) {
        return this.lambdaUpdate().in(SysDictItemDO::getDid, dids).remove();
    }
}
