package com.nuts.fast.module.sys.model.vo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 系统管理 - 系统参数信息保存请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysParamSaveReqVO", description = "系统管理 - 系统参数信息保存请求 VO")
public class SysParamSaveReqVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long id;

    /**
     * 参数名称
     */
    @Schema(name = "paramName", description = "参数名称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String paramName;

    /**
     * 参数键
     */
    @Schema(name = "paramKey", description = "参数键", requiredMode = Schema.RequiredMode.REQUIRED)
    private String paramKey;

    /**
     * 参数值
     */
    @Schema(name = "paramValue", description = "参数值", requiredMode = Schema.RequiredMode.REQUIRED)
    private String paramValue;

    /**
     * 参数类型
     */
    @Schema(name = "paramType", description = "参数类型", requiredMode = Schema.RequiredMode.REQUIRED)
    private String paramType;

    /**
     * 是否系统内置
     */
    @Schema(name = "isBuildIn", description = "是否系统内置", requiredMode = Schema.RequiredMode.REQUIRED)
    private String isBuildIn;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
}
