package com.nuts.fast.module.sys.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.module.sys.model.entity.SysDictItemDO;

import java.io.Serializable;
import java.util.Collection;

/**
 * 系统管理 - 字典项信息服务接口类
 *
 * @author Nuts
 */
public interface ISysDictItemService extends IService<SysDictItemDO> {

    /**
     * 根据字典主键删除字典项信息
     *
     * @param did 字典主键
     * @return 成功标志
     */
    boolean removeByDid(Serializable did);

    /**
     * 根据字典主键列表删除字典项信息
     *
     * @param dids 字典主键列表
     * @return 成功标志
     */
    boolean removeByDids(Collection<?> dids);
}
