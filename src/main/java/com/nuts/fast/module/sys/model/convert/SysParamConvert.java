package com.nuts.fast.module.sys.model.convert;

import com.nuts.fast.module.sys.model.entity.SysParamDO;
import com.nuts.fast.module.sys.model.vo.param.SysParamSaveReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * 系统参数信息 POJO 转换工具
 *
 * @author Nuts
 */
@Mapper(componentModel = "spring")
public interface SysParamConvert {

    SysParamConvert INSTANCE = Mappers.getMapper(SysParamConvert.class);

    /**
     * 系统参数前端保存请求VO转换系统参数信息实体类工具
     *
     * @param sysParamSaveReqVO 系统参数前端保存请求VO
     * @return 系统参数信息实体类
     */
    SysParamDO saveReqVoConvert2Do(SysParamSaveReqVO sysParamSaveReqVO);

    /**
     * 系统参数前端保存请求VO转换系统参数信息实体类工具(修改使用，仅包含部分字段)
     *
     * @param sysParamDO        系统参数信息实体类
     * @param sysParamSaveReqVO 系统参数前端保存请求VO
     * @return 系统参数信息实体类
     */
    @Mappings({
            @Mapping(source = "sysParamSaveReqVO.id", target = "id"),
            @Mapping(source = "sysParamSaveReqVO.paramName", target = "paramName"),
            @Mapping(source = "sysParamSaveReqVO.paramKey", target = "paramKey"),
            @Mapping(source = "sysParamSaveReqVO.paramValue", target = "paramValue"),
            @Mapping(source = "sysParamSaveReqVO.paramType", target = "paramType"),
            @Mapping(source = "sysParamSaveReqVO.isBuildIn", target = "isBuildIn"),
            @Mapping(source = "sysParamSaveReqVO.status", target = "status"),
            @Mapping(source = "sysParamSaveReqVO.remark", target = "remark")
    })
    SysParamDO saveReqVoConvert2Do(SysParamDO sysParamDO, SysParamSaveReqVO sysParamSaveReqVO);
}
