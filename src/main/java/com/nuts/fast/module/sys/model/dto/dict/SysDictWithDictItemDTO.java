package com.nuts.fast.module.sys.model.dto.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 系统管理 - 字典及字典项 DTO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictWithDictItemDTO", description = "系统管理 - 字典及字典项 DTO")
public class SysDictWithDictItemDTO {

    /**
     * 字典名称
     */
    @Schema(name = "dictName", description = "字典名称")
    private String dictName;

    /**
     * 字典项键
     */
    @Schema(name = "dictItemKey", description = "字典项键")
    private String dictItemKey;

    /**
     * 字典项值
     */
    @Schema(name = "dictItemValue", description = "字典项值")
    private String dictItemValue;

    /**
     * Tag 类型
     */
    @Schema(name = "tagType", description = "Tag 类型")
    private String tagType;
}
