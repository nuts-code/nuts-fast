package com.nuts.fast.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nuts.fast.module.sys.model.entity.SysNoticeDO;
import com.nuts.fast.module.sys.model.vo.notice.CurrentUserNoticeVO;
import com.nuts.fast.module.sys.model.vo.notice.SysNoticeInfoResVO;
import com.nuts.fast.module.sys.model.vo.notice.SysNoticePageReqVO;
import com.nuts.fast.module.sys.model.vo.notice.SysNoticePageResVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统管理 - 通知公告信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface SysNoticeMapper extends BaseMapper<SysNoticeDO> {

    /**
     * 获取通知公告分页信息
     *
     * @param page             分页查询
     * @param sysNoticePageReq 通知公告分页查询请求体
     * @return 通知公告分页信息
     */
    IPage<SysNoticePageResVO> getSysNoticeListByPage(Page<Object> page, @Param("sysNoticePageReq") SysNoticePageReqVO sysNoticePageReq);

    /**
     * 根据通知公告主键获取详情信息
     *
     * @param id 通知公告主键
     * @return 通知公告详情信息
     */
    SysNoticeInfoResVO getSysNoticeInfoById(Long id);

    /**
     * 根据用户主键获取通知公告详情列表
     *
     * @param uid 用户主键
     * @return 通知公告详情列表
     */
    List<CurrentUserNoticeVO> getNoticeListByUid(Long uid);
}
