package com.nuts.fast.module.auth.model.vo.role;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 权限管理 - 角色分页查询请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthRolePageReqVO", description = "权限管理 - 角色分页查询请求 VO")
public class AuthRolePageReqVO implements Serializable {

    /**
     * 角色编号
     */
    @Schema(name = "roleCode", description = "角色编号")
    private String roleCode;

    /**
     * 角色名称
     */
    @Schema(name = "roleName", description = "角色名称")
    private String roleName;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}
