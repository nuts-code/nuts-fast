package com.nuts.fast.module.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuts.fast.module.auth.model.entity.AuthActionDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 权限管理 - 操作信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface AuthActionMapper extends BaseMapper<AuthActionDO> {
}
