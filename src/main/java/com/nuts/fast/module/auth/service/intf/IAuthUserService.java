package com.nuts.fast.module.auth.service.intf;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.common.core.security.NutsUserDetails;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.auth.model.dto.role.AuthRoleUserDTO;
import com.nuts.fast.module.auth.model.entity.AuthUserDO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserInfoResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageReqVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserSaveReqVO;

import java.util.List;
import java.util.Optional;

/**
 * 权限管理 - 用户信息服务接口类
 *
 * @author Nuts
 */
public interface IAuthUserService extends IService<AuthUserDO> {

    /**
     * 获取用户列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 用户列表分页信息
     */
    IPage<AuthUserPageResVO> page(PageModel<AuthUserPageReqVO> pageParams);

    /**
     * 通过用户主键获取用户详情信息
     *
     * @param id 用户主键
     * @return 用户详情信息
     */
    AuthUserInfoResVO getInfoById(Long id);

    /**
     * 保存用户信息
     *
     * @param authUserSaveReq 用户保存请求信息
     * @return 用户主键
     */
    Long saveOrUpdate(AuthUserSaveReqVO authUserSaveReq);

    /**
     * 通过主键查询用户信息
     *
     * @param id 主键
     * @return 用户信息
     */
    Optional<AuthUserDO> getAuthUserById(Long id);


    /**
     * 通过用户名获取并组装成 NutsUserDetails 信息
     *
     * @param username 用户名
     * @return NutsUserDetails
     */
    NutsUserDetails getNutsUserDetails(String username);

    /**
     * 通过角色主键获取用户列表
     *
     * @param rid 角色主键
     * @return 用户列表
     */
    List<AuthRoleUserDTO> getUserListByRoleId(Long rid);

    /**
     * 更新密码
     *
     * @param id       用户主键
     * @param password 密码
     */
    void updatePassword(Long id, String password);
}
