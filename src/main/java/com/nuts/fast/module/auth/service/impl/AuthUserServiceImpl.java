package com.nuts.fast.module.auth.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.common.core.security.NutsGrantedAuthority;
import com.nuts.fast.common.core.security.NutsUserDetails;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.auth.mapper.AuthUserMapper;
import com.nuts.fast.module.auth.model.dto.role.AuthRoleUserDTO;
import com.nuts.fast.module.auth.model.entity.AuthUserDO;
import com.nuts.fast.module.auth.model.entity.AuthUserRoleDO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserInfoResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageReqVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserSaveReqVO;
import com.nuts.fast.module.auth.service.intf.IAuthUserRoleService;
import com.nuts.fast.module.auth.service.intf.IAuthUserService;
import lombok.val;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 权限管理 - 用户信息服务实现类
 *
 * @author Nuts
 */
@Service
public class AuthUserServiceImpl extends ServiceImpl<AuthUserMapper, AuthUserDO> implements IAuthUserService {

    @Resource
    private IAuthUserRoleService authUserRoleService;

    @Resource
    private PasswordEncoder passwordEncoder;

    /**
     * 获取用户列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 用户列表分页信息
     */
    @Override
    public IPage<AuthUserPageResVO> page(PageModel<AuthUserPageReqVO> pageParams) {
        return baseMapper.getAuthUserListByPage(new Page<>(pageParams.getCurrent(), pageParams.getSize()), pageParams.getParams());
    }

    /**
     * 通过用户主键获取用户详情信息
     *
     * @param id 用户主键
     * @return 用户详情信息
     */
    @Override
    public AuthUserInfoResVO getInfoById(Long id) {

        return Optional.ofNullable(baseMapper.getAuthUserInfoById(id))
                .orElseThrow(() -> new RuntimeException("获取用户信息失败, 不存在当前用户信息!"));
    }

    /**
     * 保存用户信息
     *
     * @param authUserSaveReq 用户保存请求信息
     * @return 用户主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveOrUpdate(AuthUserSaveReqVO authUserSaveReq) {
        Long id = authUserSaveReq.getId();

        if (ObjectUtil.isEmpty(id)) {
            id = insertAuthUser(authUserSaveReq);
        } else {
            updateAuthUser(authUserSaveReq);
        }
        return id;
    }

    /**
     * 根据用户主键删除用户信息
     *
     * @param id 用户主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    /**
     * 根据用户主键列表批量删除用户信息
     *
     * @param ids 用户主键列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeByIds(Collection<?> ids) {
        return super.removeByIds(ids);
    }

    /**
     * 通过主键查询用户信息
     *
     * @param id 主键
     * @return 用户信息
     */
    @Override
    public Optional<AuthUserDO> getAuthUserById(Long id) {
        return Optional.ofNullable(baseMapper.selectById(id));
    }

    /**
     * 通过用户名获取并组装成 NutsUserDetails 信息
     *
     * @param username 用户名
     * @return NutsUserDetails
     */
    @Override
    public NutsUserDetails getNutsUserDetails(String username) {

        val qw = new LambdaQueryWrapper<AuthUserDO>();

        return Optional.ofNullable(baseMapper.selectOne(qw.eq(AuthUserDO::getUsername, username)))
                .map(authUser -> NutsUserDetails.builder()
                        .userId(authUser.getId())
                        .username(authUser.getUsername())
                        .password(authUser.getPassword())
                        .registerTime(authUser.getCreatedTime())
                        .enabled(0 != authUser.getStatus())
                        .accountNonLocked(2 != authUser.getStatus())
                        .authorities(List.of(
                                NutsGrantedAuthority.builder().authority("AUTH_ACCESS").build(),
                                NutsGrantedAuthority.builder().authority("ROLE_ACTIVITI_USER").build()))
                        .build())
                .orElseThrow(() -> new UsernameNotFoundException("未找到对应用户信息"));
    }

    /**
     * 通过角色主键获取用户列表
     *
     * @param rid 角色主键
     * @return 用户列表
     */
    @Override
    public List<AuthRoleUserDTO> getUserListByRoleId(Long rid) {
        return baseMapper.getAuthUserListByRoleId(rid);
    }


    /**
     * 更新密码
     *
     * @param id       用户主键
     * @param password 密码
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updatePassword(Long id, String password) {

        val encodePassword = passwordEncoder.encode(password);

        val oldUser = baseMapper.selectById(id);

        baseMapper.updateById(oldUser.withPassword(encodePassword));
    }

    /**
     * 新增用户信息
     *
     * @param authUserSaveReq 用户保存请求信息
     * @return 用户主键
     */
    private Long insertAuthUser(AuthUserSaveReqVO authUserSaveReq) {

        val password = passwordEncoder.encode("1qaz!QAZ");

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val user = AuthUserDO.builder()
                .username(authUserSaveReq.getUsername())
                .password(password)
                .nickname(authUserSaveReq.getNickname())
                .gender(authUserSaveReq.getGender())
                .mobile(authUserSaveReq.getMobile())
                .email(authUserSaveReq.getEmail())
                .errorTimes(0)
                .status(authUserSaveReq.getStatus())
                .remark(authUserSaveReq.getRemark())
                .createdBy(currentUserId)
                .createdTime(new Date())
                .updatedBy(currentUserId)
                .updatedTime(new Date())
                .build();

        baseMapper.insert(user);

        val uid = user.getId();

        authUserSaveReq.getInsertRoleIds().forEach(rid ->
                authUserRoleService.save(AuthUserRoleDO.builder()
                        .uid(uid)
                        .rid(rid)
                        .build())
        );

        return uid;
    }

    /**
     * 修改用户信息
     *
     * @param authUserSaveReq 用户保存请求信息
     */
    private void updateAuthUser(AuthUserSaveReqVO authUserSaveReq) {

        val oldUser = baseMapper.selectById(authUserSaveReq.getId());

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val user = oldUser
                .withUsername(authUserSaveReq.getUsername())
                .withNickname(authUserSaveReq.getNickname())
                .withGender(authUserSaveReq.getGender())
                .withMobile(authUserSaveReq.getMobile())
                .withEmail(authUserSaveReq.getEmail())
                .withStatus(authUserSaveReq.getStatus())
                .withRemark(authUserSaveReq.getRemark())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        baseMapper.updateById(user);

        val uid = user.getId();

        authUserSaveReq.getInsertRoleIds().forEach(rid ->
                authUserRoleService.save(AuthUserRoleDO.builder()
                        .uid(uid)
                        .rid(rid)
                        .build())
        );

        authUserSaveReq.getRemoveRoleIds().forEach(rid ->
                authUserRoleService.lambdaUpdate()
                        .eq(AuthUserRoleDO::getRid, rid)
                        .eq(AuthUserRoleDO::getUid, uid)
                        .remove()
        );
    }
}
