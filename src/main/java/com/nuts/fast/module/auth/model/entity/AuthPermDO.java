package com.nuts.fast.module.auth.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 权限管理 - 权限信息实体类
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthPermDO", description = "权限管理 - 权限信息, 包括前端权限(菜单、操作)")
@TableName("auth_perm")
public class AuthPermDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 权限所属 id
     * 权限所属方的 id,如用户 id、角色 id
     */
    @Schema(name = "belongId", description = "权限所属方的id, 如用户id、角色id")
    private Long belongId;

    /**
     * 权限主体 id
     * 权限主体的 id,如菜单 id
     */
    @Schema(name = "subjectId", description = "权限主体的id,如菜单id")
    private Long subjectId;

    /**
     * 权限所属类型
     * 0 - 角色, 1 - 用户, 2 - 部门
     */
    @Schema(name = "belongType", description = "权限所属方的类型: 0 - 角色, 1 - 用户, 2 - 部门")
    private Integer belongType;

    /**
     * 权限主体类型
     * 0 - 菜单, 1 - 操作
     */
    @Schema(name = "subjectType", description = "权限主体的类型: 0 - 菜单, 1 - 操作")
    private Integer subjectType;
}
