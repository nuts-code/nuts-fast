package com.nuts.fast.module.auth.model.vo.user;

import com.nuts.fast.common.data.annotation.Desensitize;
import com.nuts.fast.common.data.enums.DesensitizeMethodEnum;
import com.nuts.fast.common.data.enums.DesensitizeTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户管理 - 用户分页信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthUserPageResVO", description = "用户管理 - 用户分页信息返回 VO")
public class AuthUserPageResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private String id;

    /**
     * 用户名
     */
    @Schema(name = "username", description = "用户名")
    private String username;

    /**
     * 昵称
     */
    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    /**
     * 性别
     */
    @Schema(name = "gender", description = "性别")
    private Integer gender;

    /**
     * 手机号
     */
    @Desensitize(
            method = {DesensitizeMethodEnum.SERIALIZE, DesensitizeMethodEnum.LOG},
            type = DesensitizeTypeEnum.PHONE
    )
    @Schema(name = "mobile", description = "手机号")
    private String mobile;

    /**
     * 电子邮箱
     */
    @Desensitize(
            method = {DesensitizeMethodEnum.SERIALIZE, DesensitizeMethodEnum.LOG},
            type = DesensitizeTypeEnum.EMAIL
    )
    @Schema(name = "email", description = "电子邮箱")
    private String email;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "createdByName", description = "创建人")
    private String createdByName;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedByName", description = "更新人")
    private String updatedByName;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}
