package com.nuts.fast.module.auth.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.module.auth.model.entity.AuthUserRoleDO;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 权限管理 - 用户与角色关系服务接口类
 *
 * @author Nuts
 */
public interface IAuthUserRoleService extends IService<AuthUserRoleDO> {

    /**
     * 通过用户 id 获取角色 id 列表
     *
     * @param userId 用户 id
     * @return 角色 id 列表
     */
    Optional<Set<Long>> getRoleIdListByUserId(long userId);

    /**
     * 通过角色 id 获取用户 id 列表
     *
     * @param roleId 角色 id
     * @return 用户 id 列表
     */
    List<Long> getUserIdListByRoleId(long roleId);
}
