package com.nuts.fast.module.auth.service.intf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.auth.model.dto.user.AuthUserRoleDTO;
import com.nuts.fast.module.auth.model.vo.role.*;

import java.util.List;

/**
 * 权限管理 - 角色信息服务接口类
 *
 * @author Nuts
 */
public interface IAuthRoleService {

    /**
     * 获取角色列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 角色列表分页信息
     */
    IPage<AuthRolePageResVO> getAuthRoleListByPage(PageModel<AuthRolePageReqVO> pageParams);

    /**
     * 通过角色主键获取角色详情信息
     *
     * @param id 角色主键
     * @return 角色详情信息
     */
    AuthRoleInfoResVO getAuthRoleInfoById(Long id);

    /**
     * 保存角色信息
     *
     * @param authRoleSaveReq 角色保存请求信息
     * @return 角色主键
     */
    Long saveAuthRole(AuthRoleSaveReqVO authRoleSaveReq);

    /**
     * 根据角色主键删除角色信息
     *
     * @param id 角色主键
     */
    void deleteAuthRoleById(Long id);

    /**
     * 根据角色主键列表批量删除角色信息
     *
     * @param ids 角色主键列表
     */
    void deleteAuthRoleByIds(List<Long> ids);

    /**
     * 获取全部角色列表
     *
     * @return 全部角色列表
     */
    List<AuthRoleForUserResVO> getAllRoleList();

    /**
     * 根据用户主键获取全部角色信息列表
     *
     * @param uid 用户主键
     * @return 角色信息列表
     */
    List<AuthUserRoleDTO> getRoleListByUserId(Long uid);
}
