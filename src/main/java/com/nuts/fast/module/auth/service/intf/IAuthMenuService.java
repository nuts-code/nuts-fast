package com.nuts.fast.module.auth.service.intf;

import cn.hutool.core.lang.tree.Tree;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuInfoResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListReqVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuSaveReqVO;

import java.util.List;
import java.util.Optional;

/**
 * 权限管理 - 菜单信息服务接口类
 *
 * @author Nuts
 */
public interface IAuthMenuService {

    /**
     * 获取全部菜单树
     *
     * @return 菜单树
     */
    List<Tree<Long>> getAllAuthMenuTree();

    /**
     * 获取全部菜单树(包含操作)
     *
     * @return 菜单树(包含操作)
     */
    List<Tree<Long>> getAllAuthMenuWithActionTree();

    /**
     * 通过菜单 id 列表组装菜单树
     *
     * @param menuIds 菜单 id 列表
     * @return 菜单树
     */
    Optional<List<Tree<Long>>> generateMenuTreeByMenuIds(List<Long> menuIds);

    /**
     * 获取菜单列表信息
     *
     * @param authMenuPageReq 查询参数
     * @return 菜单列表分页信息
     */
    List<AuthMenuListResVO> getAuthMenuList(AuthMenuListReqVO authMenuPageReq);

    /**
     * 通过菜单主键获取菜单详情信息
     *
     * @param id 菜单主键
     * @return 菜单详情信息
     */
    AuthMenuInfoResVO getAuthMenuInfoById(Long id);

    /**
     * 保存菜单信息
     *
     * @param authMenuSaveReq 菜单保存请求信息
     * @return 菜单主键
     */
    Long saveAuthMenu(AuthMenuSaveReqVO authMenuSaveReq);

    /**
     * 根据菜单主键删除菜单信息
     *
     * @param id 菜单主键
     */
    void deleteAuthMenuById(Long id);

    /**
     * 根据菜单主键列表批量删除菜单信息
     *
     * @param ids 菜单主键列表
     */
    void deleteAuthMenuByIds(List<Long> ids);
}
