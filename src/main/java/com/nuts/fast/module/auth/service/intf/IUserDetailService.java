package com.nuts.fast.module.auth.service.intf;

import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

/**
 * 权限管理 - 用户详情信息服务接口类 (Spring Security)
 *
 * @author Nuts
 */
public interface IUserDetailService extends UserDetailsService {

    /**
     * 系统登录
     *
     * @param username 用户名
     * @param password 密码
     * @return 登陆 token 信息
     */
    Optional<String> login(String username, String password);

    /**
     * 系统登出
     *
     * @param token 登陆 token 信息
     */
    void logout(String token);
}
