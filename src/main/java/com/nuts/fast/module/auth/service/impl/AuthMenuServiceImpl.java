package com.nuts.fast.module.auth.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.module.auth.mapper.AuthActionMapper;
import com.nuts.fast.module.auth.mapper.AuthMenuMapper;
import com.nuts.fast.module.auth.model.dto.menu.AuthMenuShowDTO;
import com.nuts.fast.module.auth.model.entity.AuthActionDO;
import com.nuts.fast.module.auth.model.entity.AuthMenuDO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuInfoResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListReqVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuSaveReqVO;
import com.nuts.fast.module.auth.service.intf.IAuthMenuService;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 权限管理 - 菜单信息服务实现类
 *
 * @author Nuts
 */
@Service
public class AuthMenuServiceImpl implements IAuthMenuService {

    @Resource
    private AuthMenuMapper authMenuMapper;

    @Resource
    private AuthActionMapper authActionMapper;

    /**
     * 获取全部菜单树
     *
     * @return 菜单树
     */
    @Override
    public List<Tree<Long>> getAllAuthMenuTree() {

        val menus = authMenuMapper.selectList(new LambdaQueryWrapper<>());

        val nodeList = menus.stream()
                .map(menu -> {

                    val node = new TreeNode<>(
                            menu.getId(),
                            menu.getPid(),
                            menu.getMenuName(),
                            menu.getSort()
                    );

                    node.setExtra(Map.of(
                            "title", menu.getMenuName(),
                            "key", menu.getId())
                    );

                    return node;
                })
                .collect(Collectors.toList());

        return TreeUtil.build(nodeList, 0L);
    }

    /**
     * 获取全部菜单树(包含操作)
     *
     * @return 菜单树(包含操作)
     */
    @Override
    public List<Tree<Long>> getAllAuthMenuWithActionTree() {

        val menus = authMenuMapper.selectList(new LambdaQueryWrapper<>());
        val actions = authActionMapper.selectList(new LambdaQueryWrapper<>());

        val nodeList = menus.stream()
                .map(menu -> {

                    val node = new TreeNode<>(
                            menu.getId(),
                            menu.getPid(),
                            menu.getMenuName(),
                            menu.getSort()
                    );

                    val currentActions = actions.stream()
                            .filter(action -> action.getMid().equals(menu.getId()));

                    node.setExtra(Map.of(
                            "title", menu.getMenuName(),
                            "key", menu.getId(),
                            "actions", currentActions)
                    );

                    return node;
                })
                .collect(Collectors.toList());

        return TreeUtil.build(nodeList, 0L);
    }

    /**
     * 通过菜单 id 列表组装菜单树
     *
     * @param menuIds 菜单 id 列表
     * @return 菜单树
     */
    @Override
    public Optional<List<Tree<Long>>> generateMenuTreeByMenuIds(List<Long> menuIds) {

        val qwMenu = new LambdaQueryWrapper<AuthMenuDO>();

        val menus =
                authMenuMapper.selectList(qwMenu
                        .eq(AuthMenuDO::getStatus, 1)
                        .in(AuthMenuDO::getId, menuIds)
                );

        val nodeList = menus.stream()
                .map(menu -> {
                    TreeNode<Long> node = new TreeNode<>(
                            menu.getId(),
                            menu.getPid(),
                            menu.getMenuName(),
                            menu.getSort()
                    );
                    val authMenuDTO = AuthMenuShowDTO.builder()
                            .code(menu.getMenuCode())
                            .path(menu.getPath())
                            .icon(menu.getIcon())
                            .showInBread(menu.getShowInBread())
                            .visible(menu.getVisible())
                            .keepLive(menu.getKeepLive())
                            .scheme(menu.getScheme())
                            .target(menu.getTarget())
                            .build();
                    node.setExtra(BeanUtil.beanToMap(authMenuDTO));
                    return node;
                })
                .collect(Collectors.toList());
        if (nodeList.size() > 0) {
            return Optional.of(TreeUtil.build(nodeList, 0L));
        } else {
            return Optional.empty();
        }
    }

    /**
     * 获取菜单列表信息
     *
     * @param authMenuListReq 查询参数
     * @return 菜单列表分页信息
     */
    @Override
    public List<AuthMenuListResVO> getAuthMenuList(AuthMenuListReqVO authMenuListReq) {
        return authMenuMapper.getAuthMenuList(authMenuListReq);
    }

    /**
     * 通过菜单主键获取菜单详情信息
     *
     * @param id 菜单主键
     * @return 菜单详情信息
     */
    @Override
    public AuthMenuInfoResVO getAuthMenuInfoById(Long id) {

        val qw = new LambdaQueryWrapper<AuthActionDO>();

        val actions = authActionMapper.selectList(qw.eq(AuthActionDO::getMid, id));

        return Optional.ofNullable(authMenuMapper.getAuthMenuInfoById(id).withActions(actions))
                .orElseThrow(() -> new RuntimeException("获取菜单信息失败, 不存在当前菜单信息!"));
    }

    /**
     * 保存菜单信息
     *
     * @param authMenuSaveReq 菜单保存请求信息
     * @return 菜单主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveAuthMenu(AuthMenuSaveReqVO authMenuSaveReq) {
        Long id = authMenuSaveReq.getId();

        if (ObjectUtil.isEmpty(id)) {
            id = insertAuthMenu(authMenuSaveReq);
        } else {
            updateAuthMenu(authMenuSaveReq);
        }
        return id;
    }

    /**
     * 根据菜单主键删除菜单信息
     *
     * @param id 菜单主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAuthMenuById(Long id) {
        authMenuMapper.deleteById(id);
    }

    /**
     * 根据菜单主键列表批量删除菜单信息
     *
     * @param ids 菜单主键列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAuthMenuByIds(List<Long> ids) {
        authMenuMapper.deleteBatchIds(ids);
    }

    /**
     * 新增菜单信息
     *
     * @param authMenuSaveReq 菜单保存请求信息
     * @return 菜单主键
     */
    private Long insertAuthMenu(AuthMenuSaveReqVO authMenuSaveReq) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val menu = AuthMenuDO.builder()
                .pid(authMenuSaveReq.getPid())
                .menuCode(authMenuSaveReq.getMenuCode())
                .menuName(authMenuSaveReq.getMenuName())
                .icon(authMenuSaveReq.getIcon())
                .scheme(authMenuSaveReq.getScheme())
                .target(authMenuSaveReq.getTarget())
                .path(authMenuSaveReq.getPath())
                .visible(authMenuSaveReq.getVisible())
                .showInBread(authMenuSaveReq.getShowInBread())
                .sort(authMenuSaveReq.getSort())
                .keepLive(authMenuSaveReq.getKeepLive())
                .status(authMenuSaveReq.getStatus())
                .remark(authMenuSaveReq.getRemark())
                .createdBy(currentUserId)
                .createdTime(new Date())
                .updatedBy(currentUserId)
                .updatedTime(new Date())
                .build();

        authMenuMapper.insert(menu);

        val id = menu.getId();

        authMenuSaveReq.getInsertActions().forEach(action -> authActionMapper.insert(action.withMid(id)));

        return id;
    }

    /**
     * 修改菜单信息
     *
     * @param authMenuSaveReq 菜单保存请求信息
     */
    private void updateAuthMenu(AuthMenuSaveReqVO authMenuSaveReq) {

        val oldMenu = authMenuMapper.selectById(authMenuSaveReq.getId());

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val menu = oldMenu
                .withPid(authMenuSaveReq.getPid())
                .withMenuCode(authMenuSaveReq.getMenuCode())
                .withMenuName(authMenuSaveReq.getMenuName())
                .withIcon(authMenuSaveReq.getIcon())
                .withScheme(authMenuSaveReq.getScheme())
                .withTarget(authMenuSaveReq.getTarget())
                .withPath(authMenuSaveReq.getPath())
                .withVisible(authMenuSaveReq.getVisible())
                .withShowInBread(authMenuSaveReq.getShowInBread())
                .withSort(authMenuSaveReq.getSort())
                .withKeepLive(authMenuSaveReq.getKeepLive())
                .withStatus(authMenuSaveReq.getStatus())
                .withRemark(authMenuSaveReq.getRemark())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        authMenuMapper.updateById(menu);

        val id = menu.getId();

        // 新增操作列表
        val insertActions = authMenuSaveReq.getInsertActions();
        if (insertActions != null && insertActions.size() != 0) {
            insertActions.forEach(action -> authActionMapper.insert(action.withMid(id)));
        }

        // 更新操作列表
        val updateActions = authMenuSaveReq.getUpdateActions();
        if (updateActions != null && updateActions.size() != 0) {
            updateActions.forEach(action -> authActionMapper.updateById(action.withMid(id)));
        }

        // 删除操作列表
        val removeActions = authMenuSaveReq.getRemoveActions();
        if (removeActions != null && removeActions.size() != 0) {
            removeActions.forEach(action -> authActionMapper.deleteById(action));
        }
    }
}
