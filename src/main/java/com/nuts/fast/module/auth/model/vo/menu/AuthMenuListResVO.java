package com.nuts.fast.module.auth.model.vo.menu;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "权限管理 - 菜单信息", description = "权限管理 - 菜单信息")
public class AuthMenuListResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 父菜单主键
     */
    @Schema(name = "pid", description = "父菜单主键")
    private Long pid;

    /**
     * 菜单名称
     */
    @Schema(name = "menuName", description = "菜单名称")
    private String menuName;

    /**
     * 菜单图标
     */
    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    /**
     * 组件路径
     */
    @Schema(name = "path", description = "组件路径")
    private String path;

    /**
     * 是否在菜单中可见
     */
    @Schema(name = "visible", description = "是否在菜单中可见")
    private String visible;

    /**
     * 是否在面包屑中显示
     */
    @Schema(name = "showInBread", description = "是否在面包屑中显示")
    private String showInBread;

    /**
     * 排序
     */
    @Schema(name = "sort", description = "排序")
    private Integer sort;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 创建人
     */
    @Schema(name = "createdByName", description = "创建人")
    private String createdByName;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedByName", description = "更新人")
    private String updatedByName;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
}
