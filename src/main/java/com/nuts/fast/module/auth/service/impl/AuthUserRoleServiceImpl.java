package com.nuts.fast.module.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.module.auth.mapper.AuthUserRoleMapper;
import com.nuts.fast.module.auth.model.entity.AuthUserRoleDO;
import com.nuts.fast.module.auth.service.intf.IAuthUserRoleService;
import lombok.val;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 权限管理 - 用户与角色关系服务实现类
 *
 * @author Nuts
 */
@Service
public class AuthUserRoleServiceImpl extends ServiceImpl<AuthUserRoleMapper, AuthUserRoleDO> implements IAuthUserRoleService {

    /**
     * 通过用户 id 获取角色 id 列表
     *
     * @param userId 用户 id
     * @return 角色 id 列表
     */
    @Override
    public Optional<Set<Long>> getRoleIdListByUserId(long userId) {

        val authUserRoles = this.lambdaQuery().eq(AuthUserRoleDO::getUid, userId).list();

        if (authUserRoles.size() > 0) {
            return Optional.of(authUserRoles.stream()
                    .map(AuthUserRoleDO::getRid)
                    .collect(Collectors.toSet()));
        }

        return Optional.empty();
    }

    /**
     * 通过角色 id 获取用户 id 列表
     *
     * @param roleId 角色 id
     * @return 用户 id 列表
     */
    @Override
    public List<Long> getUserIdListByRoleId(long roleId) {

        val authUserRoles = this.lambdaQuery().eq(AuthUserRoleDO::getRid, roleId).list();

        if (authUserRoles.size() > 0) {
            return authUserRoles.stream()
                    .map(AuthUserRoleDO::getRid)
                    .collect(Collectors.toList());
        }

        return Collections.EMPTY_LIST;
    }
}
