package com.nuts.fast.module.auth.model.vo.user;

import com.nuts.fast.common.data.annotation.Desensitize;
import com.nuts.fast.common.data.enums.DesensitizeTypeEnum;
import com.nuts.fast.module.auth.model.dto.user.AuthUserRoleDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 用户管理 - 用户详情信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthUserInfoResVO", description = "用户管理 - 用户详情信息返回 VO")
public class AuthUserInfoResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private String id;

    /**
     * 用户名
     */
    @Schema(name = "username", description = "用户名")
    private String username;

    /**
     * 昵称
     */
    @Desensitize(type = DesensitizeTypeEnum.NAME)
    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    /**
     * 性别
     */
    @Schema(name = "gender", description = "性别")
    private Integer gender;

    /**
     * 手机号
     */
    @Desensitize(type = DesensitizeTypeEnum.PHONE)
    @Schema(name = "mobile", description = "手机号")
    private String mobile;

    /**
     * 电子邮箱
     */
    @Desensitize(type = DesensitizeTypeEnum.EMAIL)
    @Schema(name = "email", description = "电子邮箱")
    private String email;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 用户所属角色列表
     */
    @Schema(name = "roles", description = "用户所属角色列表")
    private List<AuthUserRoleDTO> roles;
}
