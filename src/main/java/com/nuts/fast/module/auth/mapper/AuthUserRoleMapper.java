package com.nuts.fast.module.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuts.fast.module.auth.model.entity.AuthUserRoleDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 权限管理 - 用户角色关联信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface AuthUserRoleMapper extends BaseMapper<AuthUserRoleDO> {
}
