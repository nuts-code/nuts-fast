package com.nuts.fast.module.auth.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.auth.mapper.AuthPermMapper;
import com.nuts.fast.module.auth.mapper.AuthRoleMapper;
import com.nuts.fast.module.auth.mapper.AuthUserRoleMapper;
import com.nuts.fast.module.auth.model.dto.user.AuthUserRoleDTO;
import com.nuts.fast.module.auth.model.entity.AuthPermDO;
import com.nuts.fast.module.auth.model.entity.AuthRoleDO;
import com.nuts.fast.module.auth.model.entity.AuthUserRoleDO;
import com.nuts.fast.module.auth.model.vo.role.*;
import com.nuts.fast.module.auth.service.intf.IAuthRoleService;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 权限管理 - 角色信息服务实现类
 *
 * @author Nuts
 */
@Service
public class AuthRoleServiceImpl implements IAuthRoleService {

    @Resource
    private AuthRoleMapper authRoleMapper;

    @Resource
    private AuthUserRoleMapper authUserRoleMapper;

    @Resource
    private AuthPermMapper authPermMapper;

    /**
     * 获取角色列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 角色列表分页信息
     */
    @Override
    public IPage<AuthRolePageResVO> getAuthRoleListByPage(PageModel<AuthRolePageReqVO> pageParams) {
        return authRoleMapper.getAuthRoleListByPage(new Page<>(pageParams.getCurrent(), pageParams.getSize()), pageParams.getParams());
    }

    /**
     * 通过角色主键获取角色详情信息
     *
     * @param id 角色主键
     * @return 角色详情信息
     */
    @Override
    public AuthRoleInfoResVO getAuthRoleInfoById(Long id) {
        return authRoleMapper.getAuthRoleInfoById(id);
    }

    /**
     * 保存角色信息
     *
     * @param authRoleSaveReq 角色保存请求信息
     * @return 角色主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveAuthRole(AuthRoleSaveReqVO authRoleSaveReq) {
        Long id = authRoleSaveReq.getId();

        if (ObjectUtil.isEmpty(id)) {
            id = insertAuthRole(authRoleSaveReq);
        } else {
            updateAuthRole(authRoleSaveReq);
        }
        return id;
    }

    /**
     * 根据角色主键删除角色信息
     *
     * @param id 角色主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAuthRoleById(Long id) {
        authRoleMapper.deleteById(id);
    }

    /**
     * 根据角色主键列表批量删除角色信息
     *
     * @param ids 角色主键列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAuthRoleByIds(List<Long> ids) {
        authRoleMapper.deleteBatchIds(ids);
    }

    /**
     * 获取全部角色列表
     *
     * @return 全部角色列表
     */
    @Override
    public List<AuthRoleForUserResVO> getAllRoleList() {
        return authRoleMapper.selectList(new LambdaQueryWrapper<>()).stream()
                .map(authRole -> AuthRoleForUserResVO.builder()
                        .id(authRole.getId())
                        .roleName(authRole.getRoleName())
                        .status(authRole.getStatus())
                        .build())
                .collect(Collectors.toList());
    }

    /**
     * 根据用户主键获取全部角色信息列表
     *
     * @param uid 用户主键
     * @return 角色信息列表
     */
    @Override
    public List<AuthUserRoleDTO> getRoleListByUserId(Long uid) {
        return authRoleMapper.getAuthRoleListByUserId(uid);
    }

    /**
     * 新增角色信息
     *
     * @param authRoleSaveReq 角色保存请求信息
     * @return 角色主键
     */
    private Long insertAuthRole(AuthRoleSaveReqVO authRoleSaveReq) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val role = AuthRoleDO.builder()
                .roleCode(authRoleSaveReq.getRoleCode())
                .roleName(authRoleSaveReq.getRoleName())
                .status(authRoleSaveReq.getStatus())
                .remark(authRoleSaveReq.getRemark())
                .createdBy(currentUserId)
                .createdTime(new Date())
                .updatedBy(currentUserId)
                .updatedTime(new Date())
                .build();

        authRoleMapper.insert(role);

        val roleId = role.getId();

        val userIds = authRoleSaveReq.getInsertUserIds();
        insertUserRole(userIds, roleId);

        val menuIds = authRoleSaveReq.getMenuPermIds();
        val actionIds = authRoleSaveReq.getActionPermIds();

        insertMenuAndActionPerm(menuIds, actionIds, roleId);

        return roleId;
    }

    /**
     * 修改角色信息
     *
     * @param authRoleSaveReq 角色保存请求信息
     */
    private void updateAuthRole(AuthRoleSaveReqVO authRoleSaveReq) {

        val oldRole = authRoleMapper.selectById(authRoleSaveReq.getId());

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        val role = oldRole
                .withRoleCode(authRoleSaveReq.getRoleCode())
                .withRoleName(authRoleSaveReq.getRoleName())
                .withStatus(authRoleSaveReq.getStatus())
                .withRemark(authRoleSaveReq.getRemark())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());

        authRoleMapper.updateById(role);

        val roleId = role.getId();

        val removeUserIds = authRoleSaveReq.getRemoveUserIds();
        val insertUserIds = authRoleSaveReq.getInsertUserIds();
        authUserRoleMapper.delete(new LambdaUpdateWrapper<AuthUserRoleDO>()
                .eq(AuthUserRoleDO::getRid, roleId)
                .in(AuthUserRoleDO::getUid, removeUserIds)
        );
        insertUserRole(insertUserIds, roleId);

        val menuIds = authRoleSaveReq.getMenuPermIds();
        val actionIds = authRoleSaveReq.getActionPermIds();
        authPermMapper.delete(new LambdaUpdateWrapper<AuthPermDO>()
                .eq(AuthPermDO::getBelongType, 0)
                .eq(AuthPermDO::getBelongId, roleId));

        insertMenuAndActionPerm(menuIds, actionIds, roleId);
    }

    /**
     * 新增用户角色关联信息
     *
     * @param userIds 用户主键列表
     * @param roleId  角色主键
     */
    private void insertUserRole(List<Long> userIds, Long roleId) {
        userIds.forEach(userId ->
                authUserRoleMapper.insert(AuthUserRoleDO.builder()
                        .rid(roleId)
                        .uid(userId)
                        .build()));
    }

    /**
     * 新增用户权限信息
     *
     * @param menuIds   菜单主键列表
     * @param actionIds 操作主键列表
     * @param roleId    角色主键
     */
    private void insertMenuAndActionPerm(List<Long> menuIds, List<Long> actionIds, Long roleId) {

        menuIds.forEach(menuId ->
                authPermMapper.insert(AuthPermDO.builder()
                        .belongId(roleId)
                        .subjectId(menuId)
                        .belongType(0)
                        .subjectType(0)
                        .build()));

        actionIds.forEach(actionId ->
                authPermMapper.insert(AuthPermDO.builder()
                        .belongId(roleId)
                        .subjectId(actionId)
                        .belongType(0)
                        .subjectType(1)
                        .build()));
    }
}
