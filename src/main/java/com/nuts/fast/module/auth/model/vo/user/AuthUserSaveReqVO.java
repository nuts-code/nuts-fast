package com.nuts.fast.module.auth.model.vo.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 用户管理 - 用户信息保存请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthUserSaveReqVO", description = "用户管理 - 用户信息保存请求 VO")
public class AuthUserSaveReqVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long id;

    /**
     * 用户名
     */
    @Schema(name = "username", description = "用户名", requiredMode = Schema.RequiredMode.REQUIRED)
    private String username;

    /**
     * 昵称
     */
    @Schema(name = "nickname", description = "昵称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String nickname;

    /**
     * 性别
     */
    @Schema(name = "gender", description = "性别", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer gender;

    /**
     * 手机号
     */
    @Schema(name = "mobile", description = "手机号", requiredMode = Schema.RequiredMode.REQUIRED)
    private String mobile;

    /**
     * 电子邮箱
     */
    @Schema(name = "email", description = "电子邮箱", requiredMode = Schema.RequiredMode.REQUIRED)
    private String email;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 角色主键列表
     */
    @Schema(name = "insertRoleIds", description = "角色主键列表")
    private List<Long> insertRoleIds;

    /**
     * 角色主键列表
     */
    @Schema(name = "removeRoleIds", description = "角色主键列表")
    private List<Long> removeRoleIds;
}
