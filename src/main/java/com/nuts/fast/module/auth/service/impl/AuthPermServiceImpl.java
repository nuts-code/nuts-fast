package com.nuts.fast.module.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nuts.fast.module.auth.mapper.AuthPermMapper;
import com.nuts.fast.module.auth.model.entity.AuthPermDO;
import com.nuts.fast.module.auth.service.intf.IAuthPermService;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 权限管理 - 权限信息服务实现类
 *
 * @author Nuts
 */
@Service
public class AuthPermServiceImpl implements IAuthPermService {

    @Resource
    private AuthPermMapper authPermMapper;

    /**
     * 根据角色 id 列表获取角色对应权限集合(菜单)
     *
     * @param roleIds 角色 id 集合
     * @return 权限集合
     */
    @Override
    public Optional<List<AuthPermDO>> getMenuPermSetByRoleIds(Set<Long> roleIds) {
        val qw = new LambdaQueryWrapper<AuthPermDO>();
        return Optional.ofNullable(authPermMapper.selectList(qw
                .eq(AuthPermDO::getBelongType, 0)
                .in(AuthPermDO::getBelongId, roleIds)
                .eq(AuthPermDO::getSubjectType, 0)));
    }

    /**
     * 根据角色 id 列表获取角色对应权限集合(操作)
     *
     * @param roleIds 角色 id 集合
     * @return 权限集合
     */
    @Override
    public Optional<List<AuthPermDO>> getActionPermSetByRoleIds(Set<Long> roleIds) {
        val qw = new LambdaQueryWrapper<AuthPermDO>();
        return Optional.ofNullable(authPermMapper.selectList(qw
                .eq(AuthPermDO::getBelongType, 1)
                .in(AuthPermDO::getBelongId, roleIds)
                .eq(AuthPermDO::getSubjectType, 1)));
    }

    /**
     * 根据角色 id 获取角色对应权限列表(菜单)
     *
     * @param roleId 角色 id
     * @return 权限集合
     */
    @Override
    public List<Long> getMenuPermListByRoleId(Long roleId) {
        val qw = new LambdaQueryWrapper<AuthPermDO>();
        return authPermMapper.selectList(qw
                        .eq(AuthPermDO::getBelongId, roleId)
                        .eq(AuthPermDO::getBelongType, 0)
                        .eq(AuthPermDO::getSubjectType, 0)).stream()
                .map(AuthPermDO::getSubjectId)
                .collect(Collectors.toList());
    }

    /**
     * 根据角色 id 获取角色对应权限列表(操作)
     *
     * @param roleId 角色 id
     * @return 权限集合
     */
    @Override
    public List<Long> getActionPermListByRoleId(Long roleId) {
        val qw = new LambdaQueryWrapper<AuthPermDO>();
        return authPermMapper.selectList(qw
                        .eq(AuthPermDO::getBelongId, roleId)
                        .eq(AuthPermDO::getBelongType, 0)
                        .eq(AuthPermDO::getSubjectType, 1)).stream()
                .map(AuthPermDO::getSubjectId)
                .collect(Collectors.toList());
    }
}
