package com.nuts.fast.module.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuts.fast.module.auth.model.entity.AuthMenuDO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuInfoResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListReqVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListResVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限管理 - 菜单信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface AuthMenuMapper extends BaseMapper<AuthMenuDO> {

    /**
     * 获取菜单列表信息
     *
     * @param authMenuListReq 查询参数
     * @return 菜单列表分页信息
     */
    List<AuthMenuListResVO> getAuthMenuList(@Param("authMenuListReq") AuthMenuListReqVO authMenuListReq);


    /**
     * 根据菜单主键获取用户详情信息
     *
     * @param id 菜单主键
     * @return 菜单详情信息
     */
    AuthMenuInfoResVO getAuthMenuInfoById(Long id);
}
