package com.nuts.fast.module.auth.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "权限管理-用户与角色关系表", description = "权限管理-用户与角色关系表")
@TableName("auth_user_role")
public class AuthUserRoleDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "主键", description = "")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 用户主键
     */
    @Schema(name = "用户主键", description = "")
    private Long uid;

    /**
     * 角色主键
     */
    @Schema(name = "角色主键", description = "")
    private Long rid;
}
