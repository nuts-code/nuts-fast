package com.nuts.fast.module.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nuts.fast.module.auth.model.dto.role.AuthRoleUserDTO;
import com.nuts.fast.module.auth.model.entity.AuthUserDO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserInfoResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageReqVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageResVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限管理 - 用户信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface AuthUserMapper extends BaseMapper<AuthUserDO> {

    /**
     * 获取用户分页信息
     *
     * @param page            分页查询
     * @param authUserPageReq 用户分页信息请求体
     * @return 用户分页信息
     */
    IPage<AuthUserPageResVO> getAuthUserListByPage(Page<AuthUserPageResVO> page, @Param("authUserPageReq") AuthUserPageReqVO authUserPageReq);

    /**
     * 根据用户主键获取用户详情信息
     *
     * @param id 用户主键
     * @return 用户详情信息
     */
    AuthUserInfoResVO getAuthUserInfoById(Long id);

    /**
     * 根据角色主键列表获取用户信息列表
     *
     * @param rid 角色主键
     * @return 用户信息列表
     */
    List<AuthRoleUserDTO> getAuthUserListByRoleId(Long rid);
}
