package com.nuts.fast.module.auth.model.vo.role;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 权限管理 - 角色信息保存请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthRoleSaveReqVO", description = "权限管理 - 角色信息保存请求 VO")
public class AuthRoleSaveReqVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 角色编号
     */
    @Schema(name = "roleCode", description = "角色编号", requiredMode = Schema.RequiredMode.REQUIRED)
    private String roleCode;

    /**
     * 角色名称
     */
    @Schema(name = "roleName", description = "角色名称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String roleName;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 菜单权限主键列表
     */
    @Schema(name = "menuPermIds", description = "菜单权限主键列表")
    private List<Long> menuPermIds;

    /**
     * 操作权限主键列表
     */
    @Schema(name = "actionPermIds", description = "操作权限主键列表")
    private List<Long> actionPermIds;

    /**
     * 新增角色下用户主键列表
     */
    @Schema(name = "insertUserIds", description = "新增角色下用户主键列表")
    private List<Long> insertUserIds;

    /**
     * 移除角色下用户主键列表
     */
    @Schema(name = "removeUserIds", description = "移除角色下用户主键列表")
    private List<Long> removeUserIds;
}
