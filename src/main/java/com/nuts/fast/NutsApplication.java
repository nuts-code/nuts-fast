package com.nuts.fast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Nuts 中后台管理框架启动类
 *
 * @author Nuts
 */
@SpringBootApplication
@EnableConfigurationProperties
public class NutsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NutsApplication.class, args);
    }
}
