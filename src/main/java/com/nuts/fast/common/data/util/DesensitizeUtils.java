package com.nuts.fast.common.data.util;

import ch.qos.logback.classic.spi.LoggingEvent;
import com.nuts.fast.common.data.annotation.Desensitize;
import com.nuts.fast.common.data.enums.DesensitizeMethodEnum;
import com.nuts.fast.common.data.enums.DesensitizeTypeEnum;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * 脱敏工具类(响应序列化脱敏)
 *
 * @author nuts
 */
@Slf4j
public class DesensitizeUtils {

    /**
     * 日志事件处理器
     *
     * @param event
     */
    public static LoggingEvent loggingEventOperation(LoggingEvent event, String packageScanner) {

        val args = event.getArgumentArray();

        LoggingEvent newEvent = new LoggingEvent();
        newEvent.setMessage(event.getMessage());
        newEvent.setLevel(event.getLevel());
        newEvent.setLoggerName(event.getLoggerName());
        newEvent.setThreadName(event.getThreadName());
        newEvent.setTimeStamp(event.getTimeStamp());
        newEvent.setMDCPropertyMap(event.getMDCPropertyMap());

        if (!Objects.isNull(args)) {
            val desensitizeArgs = Arrays.stream(args)
                    .map(arg -> {
                        // 通过反射的获取到参数对象
                        Class<?> argClass = arg.getClass();
                        Package classPath = argClass.getPackage();
                        // 不是目标包不处理
                        if (!Objects.isNull(classPath) &&
                                classPath.getName().startsWith(packageScanner)) {
                            Map<String, Object> entityMap;
                            // 获取字段
                            try {
                                entityMap = loop(arg, packageScanner);
                            } catch (IllegalAccessException | NoSuchMethodException | InstantiationException |
                                     InvocationTargetException e) {
                                return arg;
                            }
                            return entityMap;
                        }
                        return arg;
                    })
                    .toArray();
            newEvent.setArgumentArray(desensitizeArgs);
        }

        return newEvent;
    }

    /**
     * 递归处理自有类属性，对象相互引用的情况暂时未处理，会抛出堆栈异常。
     */
    public static Map<String, Object> loop(Object arg, String packageScanner) throws IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {

        Class<?> argClass = arg.getClass();
        Map<String, Object> entityMap = new HashMap<>(8);
        Field[] fields = argClass.getDeclaredFields();
        for (Field field : fields) {

            field.setAccessible(true);
            Class<?> fieldTypeClass = field.getType();
            Package classPath = fieldTypeClass.getPackage();
            val fieldValue = field.get(arg);
            val fieldName = field.getName();

            if (Objects.isNull(classPath)) {
                // 基本数据类型, int long等
                entityMap.put(fieldName, fieldValue);
                continue;
            }

            // 获取字段类型
            String fieldClassPath = classPath.getName();
            if (Objects.isNull(fieldValue)) {
                // 空值字段，如果确实有对象相互依赖的，一定要处理(a1==>b==>a1  调整成  a1==>b==>a2),这样a2里的b就是空的。
                entityMap.put(fieldName, null);
                continue;
            }

            if (fieldClassPath.startsWith(packageScanner)) {
                Map<String, Object> loopEntity = loop(fieldValue, packageScanner);
                entityMap.put(fieldName, loopEntity);
            } else {
                // 判断属性是否带注解
                Desensitize desensitize = AnnotationUtils.getAnnotation(field, Desensitize.class);
                if (!Objects.isNull(desensitize) && Arrays.asList(desensitize.method()).contains(DesensitizeMethodEnum.LOG)) {
                    // 带注解的进行脱敏
                    DesensitizeTypeEnum desensitizeTypeEnum = desensitize.type();
                    int prefixNoMaskLength = desensitize.prefixNoMaskLength();
                    int suffixNoMaskLength = desensitize.suffixNoMaskLength();
                    String symbol = desensitize.symbol();

                    switch (desensitizeTypeEnum) {
                        case CUSTOMER ->
                                entityMap.put(fieldName, DesensitizeUtils.desValue(fieldValue.toString(), prefixNoMaskLength, suffixNoMaskLength, symbol));
                        case PHONE -> entityMap.put(fieldName, DesensitizeUtils.hidePhone(fieldValue.toString()));
                        case EMAIL -> entityMap.put(fieldName, DesensitizeUtils.hideEmail(fieldValue.toString()));
                        case ID_CARD -> entityMap.put(fieldName, DesensitizeUtils.hideIdCard(fieldValue.toString()));
                        case NAME -> entityMap.put(fieldName, DesensitizeUtils.hideChineseName(fieldValue.toString()));
                        case PASSWORD -> entityMap.put(fieldName, DesensitizeUtils.hidePassword(fieldValue.toString()));
                        default ->
                                throw new IllegalArgumentException("unknown privacy type enum" + desensitizeTypeEnum);
                    }
                } else {
                    // 不脱敏的保存原来值
                    entityMap.put(fieldName, fieldValue);
                }
            }
        }
        return entityMap;
    }

    /**
     * 密码全部脱敏
     *
     * @param password 密码
     * @return 脱敏后密码
     */
    public static String hidePassword(String password) {

        if (password == null) {
            return null;
        }
        return "******";
    }

    /**
     * 手机号的中间四位脱敏
     *
     * @param phone 手机号
     * @return 脱敏后手机号
     */
    public static String hidePhone(String phone) {
        return phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * 邮箱地址的中间四位脱敏
     *
     * @param email 邮箱地址
     * @return 脱敏后邮箱地址
     */
    public static String hideEmail(String email) {
        return email.replaceAll("(\\w?)(\\w+)(\\w)(@\\w+\\.[a-z]+(\\.[a-z]+)?)", "$1****$3$4");
    }

    /**
     * 身份证脱敏
     *
     * @param idCard 身份证号
     * @return 脱敏后身份证号
     */
    public static String hideIdCard(String idCard) {
        return idCard.replaceAll("(\\d{4})\\d{10}(\\w{4})", "$1*****$2");
    }

    /**
     * 中文姓名脱敏, 只显示第一个汉字其他位*号, 比如 秦**
     *
     * @param name 姓名
     * @return 脱敏后中文姓名
     */
    public static String hideChineseName(String name) {
        return desValue(name, 1, 0, "*");
    }

    /**
     * 对字符串进行自定义脱敏
     *
     * @param value              需要脱敏的字符串
     * @param prefixNoMaskLength 前置不脱敏位数
     * @param suffixNoMaskLength 后置不脱敏位数
     * @param symbol             脱敏使用符号
     * @return 脱敏后字符串
     */
    public static String desValue(String value, int prefixNoMaskLength, int suffixNoMaskLength, String symbol) {

        // 若字符串为 null 则直接返回 null
        if (value == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0, n = value.length(); i < n; i++) {

            // 前置不脱敏位数赋原字符
            if (i < prefixNoMaskLength) {
                sb.append(value.charAt(i));
                continue;
            }

            // 后置不脱敏位数赋原字符
            if (i > (n - suffixNoMaskLength - 1)) {
                sb.append(value.charAt(i));
                continue;
            }

            // 拼接脱敏使用符号
            sb.append(symbol);
        }

        return sb.toString();
    }
}
