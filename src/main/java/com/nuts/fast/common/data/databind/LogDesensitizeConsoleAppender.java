package com.nuts.fast.common.data.databind;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import com.nuts.fast.common.data.util.DesensitizeUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author nuts
 */
@Slf4j
public class LogDesensitizeConsoleAppender<T> extends ConsoleAppender<T> {

    /**
     * 日志相关配置信息
     */
    @Setter
    private String packageScanner;

    /**
     * Actual writing occurs here.
     * <p>
     * Most subclasses of <code>WriterAppender</code> will need to override this
     * method.
     *
     * @param event
     * @since 0.9.0
     */
    @Override
    protected void subAppend(T event) {
        try {
            if (event instanceof LoggingEvent) {
                super.subAppend((T) DesensitizeUtils.loggingEventOperation((LoggingEvent) event, packageScanner));
            }
        } catch (Exception e) {
            log.error("log error: {}", e.getMessage());
            super.subAppend(event);
        }
    }


}
