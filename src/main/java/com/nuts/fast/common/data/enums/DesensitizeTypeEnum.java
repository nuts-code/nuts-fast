package com.nuts.fast.common.data.enums;

import lombok.Getter;

/**
 * 脱敏类型枚举类(响应序列化脱敏)
 *
 * @author nuts
 */
@Getter
public enum DesensitizeTypeEnum {

    /**
     * 自定义
     */
    CUSTOMER,

    /**
     * 姓名
     */
    NAME,

    /**
     * 身份证号
     */
    ID_CARD,

    /**
     * 手机号
     */
    PHONE,

    /**
     * 邮箱
     */
    EMAIL,

    /**
     * 密码
     */
    PASSWORD,
}
