package com.nuts.fast.common.data.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nuts.fast.common.data.databind.ResponseDesensitizeSerializer;
import com.nuts.fast.common.data.enums.DesensitizeMethodEnum;
import com.nuts.fast.common.data.enums.DesensitizeTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定数据脱敏注解(响应序列化脱敏)
 *
 * @author nuts
 */
// 作用在字段上
@Target(ElementType.FIELD)
// class文件中保留, 运行时也保留, 能通过反射读取
@Retention(RetentionPolicy.RUNTIME)
// 自定义注解
@JacksonAnnotationsInside
// 注解序列化方式
@JsonSerialize(using = ResponseDesensitizeSerializer.class)
public @interface Desensitize {

    /**
     * 脱敏方式 默认日志脱敏
     */
    DesensitizeMethodEnum[] method() default DesensitizeMethodEnum.LOG;

    /**
     * 脱敏类型
     */
    DesensitizeTypeEnum type() default DesensitizeTypeEnum.CUSTOMER;

    /**
     * 前置不脱敏位数
     */
    int prefixNoMaskLength() default 1;

    /**
     * 后置不脱敏位数
     */
    int suffixNoMaskLength() default 1;

    /**
     * 脱敏使用符号
     */
    String symbol() default "*";
}
