package com.nuts.fast.common.redis.constant;

/**
 * @author Nuts
 */
public class RedisConstants {

    public static final String JWT_TOKENS = "JwtTokens";

    public static final String DICT_MAP = "DictMap";

    public static final String DICT_MD5 = "DictMd5";

    public static final String PARAM_MAP = "ParamMap";

    public static final String PARAM_MD5 = "ParamMd5";
}
