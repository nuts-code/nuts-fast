package com.nuts.fast.common.core.security;

import com.nuts.fast.common.data.annotation.Desensitize;
import com.nuts.fast.common.data.enums.DesensitizeTypeEnum;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.List;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
public class NutsUserDetails implements UserDetails {

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    @Desensitize(type = DesensitizeTypeEnum.PASSWORD)
    private String password;

    /**
     * 注册时间
     */
    private Date registerTime;

    /**
     * 是否激活，默认激活
     */
    @Builder.Default
    private boolean enabled = true;

    /**
     * 账户是否未过期，默认未过期
     */
    @Builder.Default
    private boolean accountNonExpired = true;

    /**
     * 账户是否未锁定，默认未锁定
     */
    @Builder.Default
    private boolean accountNonLocked = true;

    /**
     * 密码是否未过期，默认未过期
     */
    @Builder.Default
    private boolean credentialsNonExpired = true;

    /**
     * 权限信息
     */
    private List<NutsGrantedAuthority> authorities;
}
