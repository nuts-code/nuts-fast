package com.nuts.fast.common.core.security;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
public class NutsGrantedAuthority implements GrantedAuthority {

    private String authority;
}
