package com.nuts.fast.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Nuts
 */
@AllArgsConstructor
public enum ResultEnum {

    /**
     * 接口调用成功
     */
    SUCCESS(200, "接口调用成功"),
    /**
     *
     */
    VALIDATE_FAILED(2002, "参数校验失败"),
    COMMON_FAILED(2003, "接口调用失败"),
    FORBIDDEN(2004, "没有权限访问资源");

    @Getter
    private final Integer code;

    @Getter
    private final String message;
}
