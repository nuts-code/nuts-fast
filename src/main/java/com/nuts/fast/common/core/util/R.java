package com.nuts.fast.common.core.util;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 通用响应体
 *
 * @author Nuts
 */
@Data
@AllArgsConstructor
public class R<T> implements Serializable {

    private Integer code;

    private String message;

    private T result;

    public static <T> R<T> ok(Integer code, String message, T result) {
        return new R<>(code, message, result);
    }

    public static <T> R<T> ok(String message, T result) {
        return R.ok(200, message, result);
    }

    public static <T> R<T> ok(T result) {
        return R.ok("操作成功", result);
    }

    public static R<?> ok() {
        return R.ok(null);
    }

    public static <T> R<T> fail(Integer code, String message, T result) {
        return new R<>(code, message, result);
    }

    public static <T> R<T> fail(String message) {
        return new R<>(99999, message, null);
    }

    public static <T> R<T> fail() {
        return R.fail("系统异常");
    }
}
