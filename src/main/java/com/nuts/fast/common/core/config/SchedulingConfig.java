package com.nuts.fast.common.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 定时调度配置类
 *
 * @author Nuts
 */
@Configuration
@EnableScheduling
public class SchedulingConfig {
}
