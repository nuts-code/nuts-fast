package com.nuts.fast.common.security.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;

/**
 * 项目权限相关配置
 *
 * @author nuts
 */
@Validated
@Configuration
@ConfigurationProperties(prefix = "nuts.security")
public class SecurityProperties {

    @Getter
    @Setter
    @Valid
    private Jwt jwt = new Jwt();

    @Getter
    @Setter
    public static class Jwt {

        /**
         * HTTP 报头的认证字段的 key
         */
        private String header = "Authorization";

        /**
         * HTTP 报头的认证字段的值的前缀
         */
        private String prefix = "Bearer ";

        /**
         * Access Token 过期时间
         */
        @Min(5000L)
        private long accessTokenExpireTime = 60 * 1000L;

        /**
         * Refresh Token 过期时间
         */
        @Min(3600000L)
        private long refreshTokenExpireTime = 30 * 24 * 3600 * 1000L;

        private String key;

        private String refreshKey;
    }
}
