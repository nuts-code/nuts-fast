package com.nuts.fast.common.security.filter;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.nuts.fast.common.core.exception.SystemException;
import com.nuts.fast.common.core.security.NutsUserDetails;
import com.nuts.fast.common.security.config.SecurityProperties;
import com.nuts.fast.common.security.util.JwtUtils;
import com.nuts.fast.common.security.util.TokenUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * 用于 JWT Token 形式的请求过滤器
 *
 * @author Nuts
 */
@Slf4j
@Component
public class JwtFilter extends OncePerRequestFilter {

    @Resource
    private SecurityProperties securityProperties;

    @Resource
    private JwtUtils jwtUtils;

    @Resource
    private TokenUtils tokenUtils;

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain chain)
            throws ServletException, IOException {
        if (checkJwtToken(request)) {
            validateToken(request, response)
                    .filter(payload -> payload.get("authorities") != null)
                    .ifPresentOrElse(this::setupSpringAuthentication, SecurityContextHolder::clearContext);
        }
        chain.doFilter(request, response);
    }

    /**
     * 解析 JWT 得到 Claims
     *
     * @param request HTTP 请求
     * @return JWT Claims
     */
    private Optional<JSONObject> validateToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        val token = request.getHeader(securityProperties.getJwt().getHeader()).replace(securityProperties.getJwt().getPrefix(), "");
        try {
            val jwtToken = tokenUtils.retrieveJwtToken(token).orElseThrow(() -> new SystemException("token已过期"));
            return jwtUtils.parsePayloads(jwtToken);
        } catch (SystemException e) {
            return Optional.empty();
        }
    }

    /**
     * 构造 Authentication
     *
     * @param payloads JWT Payloads
     */
    private void setupSpringAuthentication(JSONObject payloads) {

        val rawList = Convert.convert(new TypeReference<List<String>>() {
        }, payloads.get("authorities"));

        val authorities = rawList.stream()
                .map(Convert::toStr)
                .map(SimpleGrantedAuthority::new)
                .collect(toList());

        val userDetails = JSONUtil.toBean((JSONObject) payloads.get("userDetails"), NutsUserDetails.class);

        val authentication = new UsernamePasswordAuthenticationToken(userDetails, null, authorities);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    /**
     * 检查 JWT Token 是否在 HTTP 报头中
     *
     * @param req HTTP 请求
     * @return 是否有 JWT Token
     */
    private boolean checkJwtToken(HttpServletRequest req) {
        String authenticationHeader = req.getHeader(securityProperties.getJwt().getHeader());
        return authenticationHeader != null && authenticationHeader.startsWith(securityProperties.getJwt().getPrefix());
    }
}
