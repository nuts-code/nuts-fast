package com.nuts.fast.common.security.util;

import cn.hutool.core.util.IdUtil;
import com.nuts.fast.common.redis.constant.RedisConstants;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * 令牌缓存工具
 *
 * @author Nuts
 */
@Slf4j
@Component
public class TokenUtils {

    /**
     * 默认登录超时时间
     */
    private static final long TIME_STEP = 60 * 60 * 24L;

    @Resource
    private RedissonClient redissonClient;

    /**
     * 缓存用户信息 (以JwtToken形式)
     *
     * @param jwtToken jwtToken
     * @return 登陆 token
     */
    public String cacheJwtToken(String jwtToken) {
        val token = IdUtil.fastSimpleUUID();
        log.debug("生成 token: {}", token);
        RMapCache<String, String> cache = redissonClient.getMapCache(RedisConstants.JWT_TOKENS);
        if (!cache.containsKey(token)) {
            cache.put(token, jwtToken, TIME_STEP, TimeUnit.SECONDS);
        }

        return token;
    }

    /**
     * 读取缓存的用户信息 (以JwtToken形式)
     *
     * @param token 登陆 token
     * @return jwtToken
     */
    public Optional<String> retrieveJwtToken(String token) {
        log.debug("输入参数 token: {}", token);
        RMapCache<String, String> cache = redissonClient.getMapCache(RedisConstants.JWT_TOKENS);
        if (cache.containsKey(token)) {
            val jwtToken = cache.get(token);
            log.debug("找到 jwtToken {}", jwtToken);
            return Optional.of(jwtToken);
        }
        return Optional.empty();
    }

    /**
     * 删除缓存的用户信息
     *
     * @param token 登陆 token
     */
    public void removeJwtToken(String token) {
        log.debug("输入参数 token: {}", token);
        RMapCache<String, String> cache = redissonClient.getMapCache(RedisConstants.JWT_TOKENS);
        if (cache.containsKey(token)) {
            val jwtToken = cache.remove(token);
            log.debug("删除 jwtToken {}", jwtToken);
        }
    }
}
