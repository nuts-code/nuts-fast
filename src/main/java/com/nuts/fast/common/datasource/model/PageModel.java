package com.nuts.fast.common.datasource.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "PageModel", description = "分页查询请求模型")
public class PageModel<T> implements Serializable {

    /**
     * 查询条件
     */
    @Schema(name = "params", description = "查询条件")
    private T params;

    /**
     * 当前页数
     */
    @Schema(name = "current", description = "当前页数", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long current;

    /**
     * 页数据量
     */
    @Schema(name = "size", description = "页数据量", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long size;

    /**
     * 排序字段
     */
    @Schema(name = "order", description = "排序字段")
    private String order;

    /**
     * 排序方式
     */
    @Schema(name = "asc", description = "排序方式")
    private boolean asc;
}
