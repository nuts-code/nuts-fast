package com.nuts.fast.controller.auth;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.auth.model.vo.role.*;
import com.nuts.fast.module.auth.service.intf.IAuthPermService;
import com.nuts.fast.module.auth.service.intf.IAuthRoleService;
import com.nuts.fast.module.auth.service.intf.IAuthUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 角色信息服务接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/auth/role")
@Tag(name = "角色信息服务接口", description = "角色信息服务接口")
public class AuthRoleController {

    @Resource
    private IAuthRoleService authRoleService;

    @Resource
    private IAuthPermService authPermService;

    @Resource
    private IAuthUserService authUserService;

    /**
     * 获取全部角色列表
     *
     * @return 角色列表
     */
    @GetMapping("/list/all")
    @Operation(summary = "获取全部角色列表", description = "获取全部角色列表")
    public List<AuthRoleForUserResVO> getAllRoleList() {
        return authRoleService.getAllRoleList();
    }

    /**
     * 获取角色分页列表
     *
     * @param pageParams 分页查询请求
     * @return 角色分页列表
     */
    @PostMapping("/list/page")
    @Operation(summary = "获取角色分页列表", description = "获取角色分页列表")
    public IPage<AuthRolePageResVO> getAuthRoleListByPage(@RequestBody PageModel<AuthRolePageReqVO> pageParams) {
        return authRoleService.getAuthRoleListByPage(pageParams);
    }


    /**
     * 获取角色详情信息
     *
     * @param id 角色 Id
     * @return 角色详情信息
     */
    @GetMapping("/info")
    @Operation(summary = "获取角色详情信息", description = "获取角色详情信息")
    @Parameters(@Parameter(name = "id", required = true, description = "角色主键"))
    public AuthRoleInfoResVO getAuthRoleInfo(@Valid @NotNull Long id) {

        val users = authUserService.getUserListByRoleId(id);

        return authRoleService.getAuthRoleInfoById(id)
                .withMenuPermIds(authPermService.getMenuPermListByRoleId(id))
                .withActionPermIds(authPermService.getActionPermListByRoleId(id))
                .withUsers(users);
    }

    /**
     * 保存角色信息
     *
     * @param authRoleSaveReq 角色保存请求信息
     * @return 角色主键
     */
    @PostMapping("/save")
    @Operation(summary = "保存角色信息", description = "保存角色信息")
    public Long saveAuthRole(@Valid @RequestBody AuthRoleSaveReqVO authRoleSaveReq) {
        return authRoleService.saveAuthRole(authRoleSaveReq);
    }

    /**
     * 根据角色主键删除角色信息
     *
     * @param id 角色主键
     */
    @PostMapping("/delete")
    @Operation(summary = "根据角色主键删除角色信息", description = "根据角色主键删除角色信息")
    @Parameters(@Parameter(name = "id", required = true, description = "角色主键"))
    public void deleteAuthRoleById(@Valid @NotNull Long id) {
        authRoleService.deleteAuthRoleById(id);
    }

    /**
     * 根据角色主键列表批量删除角色信息
     *
     * @param ids 角色主键列表
     */
    @PostMapping("/delete/batch")
    @Operation(summary = "根据角色主键列表批量删除角色信息", description = "根据角色主键列表批量删除角色信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "角色主键列表"))
    public void deleteAuthRoleById(@RequestBodyBySingleParam List<Long> ids) {
        authRoleService.deleteAuthRoleByIds(ids);
    }
}
