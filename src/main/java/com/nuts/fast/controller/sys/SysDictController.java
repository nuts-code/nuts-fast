package com.nuts.fast.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.sys.model.vo.dict.*;
import com.nuts.fast.module.sys.service.intf.ISysDictService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 字典信息服务接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/sys/dict")
@Tag(name = "字典信息服务接口", description = "字典信息服务接口")
public class SysDictController {

    @Resource
    private ISysDictService sysDictService;

    /**
     * 获取字典分页列表
     *
     * @param pageParams 分页查询请求
     * @return 字典分页列表
     */
    @PostMapping("/list/page")
    @Operation(summary = "获取字典分页列表", description = "获取字典分页列表")
    public IPage<SysDictPageResVO> getSysDictListByPage(@RequestBody PageModel<SysDictPageReqVO> pageParams) {
        return sysDictService.page(pageParams);
    }


    /**
     * 获取字典详情信息
     *
     * @param id 字典 Id
     * @return 字典详情信息
     */
    @GetMapping("/info")
    @Operation(summary = "获取字典详情信息", description = "获取字典详情信息")
    @Parameters(@Parameter(name = "id", required = true, description = "字典主键"))
    public SysDictInfoResVO getSysDictInfo(@Valid @NotNull Long id) {
        return sysDictService.getInfoById(id);
    }

    /**
     * 获取所有生效的字典下字典项
     *
     * @return 字典及字典项列表
     */
    @GetMapping("/all")
    @Operation(summary = "获取所有生效的字典下字典项", description = "获取所有生效的字典下字典项")
    public SysDictMapResVO getDictWithDictItemList() {
        return SysDictMapResVO.builder().dictMap(sysDictService.getDictWithDictItemList()).build();
    }

    /**
     * 保存字典信息
     *
     * @param sysDictSaveReq 字典保存请求信息
     * @return 字典主键
     */
    @PostMapping("/save")
    @Operation(summary = "保存字典信息", description = "保存字典信息")
    public Long saveSysDict(@Valid @RequestBody SysDictSaveReqVO sysDictSaveReq) {
        return sysDictService.saveOrUpdate(sysDictSaveReq);
    }

    /**
     * 根据字典主键删除字典信息
     *
     * @param id 字典主键
     */
    @PostMapping("/delete")
    @Operation(summary = "根据字典主键删除字典信息", description = "根据字典主键删除字典信息")
    @Parameters(@Parameter(name = "id", required = true, description = "字典主键"))
    public void deleteSysDictById(@Valid @NotNull Long id) {
        sysDictService.removeById(id);
    }

    /**
     * 根据字典主键列表批量删除字典信息
     *
     * @param ids 字典主键列表
     */
    @PostMapping("/delete/batch")
    @Operation(summary = "根据字典主键列表批量删除字典信息", description = "根据字典主键列表批量删除字典信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "字典主键列表"))
    public void deleteSysDictById(@RequestBodyBySingleParam List<Long> ids) {
        sysDictService.removeByIds(ids);
    }

    /**
     * 从数据库中加载缓存
     */
    @PostMapping("/refresh")
    @Operation(summary = "刷新缓存", description = "刷新缓存")
    public void refreshSysParamCache() {
        sysDictService.updateDictMapCache();
    }
}
