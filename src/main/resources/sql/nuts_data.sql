insert into auth_user
values (1, 'admin', '$2a$10$A7EHximvrsa4ESX1uSlkJupbg2PLO2StzDzy67NX4YV25MxmbGvXu', '超级管理员', 0,
        '18888888880', 'admin@163.com', 0, null, 1, null, 1, current_timestamp, 1, current_timestamp);
insert into auth_user
values (2, 'nuts', '$2a$10$A7EHximvrsa4ESX1uSlkJupbg2PLO2StzDzy67NX4YV25MxmbGvXu', 'Nuts', 0,
        '18888888881', 'nuts@163.com', 0, null, 1, null, 1, current_timestamp, 1, current_timestamp);

insert into auth_role
values (1, 'SYSTEM_ADMIN', '管理员', 1, '', 1, current_timestamp, 1, current_timestamp);
insert into auth_role
values (2, 'COMMON_STAFF', '普通员工', 1, '', 1, current_timestamp, 1, current_timestamp);
insert into auth_role
values (3, 'ACTIVITI_ADMIN', '流程管理员', 1, '', 1, current_timestamp, 1, current_timestamp);
insert into auth_role
values (4, 'ACTIVITI_USER', '工作流用户', 1, '', 1, current_timestamp, 1, current_timestamp);

insert into auth_user_role
values (1, 1, 1);

insert into auth_menu
values (1, 0, 'sys', '系统功能', 'IconTool', 0, 0, 'PARENT_LAYOUT', 'Y', 'Y', 0, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (10, 1, 'notice', '系统公告', 'IconMessage', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 0, 'N', 1, null, 1,
        current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (100, 10, 'search', '系统公告列表', '', 0, 0, 'sys/notice/notice-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (101, 10, 'add', '新增系统公告', '', 0, 0, 'sys/notice/notice-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (102, 10, 'edit', '编辑系统公告', '', 0, 0, 'sys/notice/notice-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (11, 1, 'param', '系统参数', 'IconList', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 1, 'N', 1, null, 1, current_timestamp,
        1,
        current_timestamp);
insert into auth_menu
values (110, 11, 'search', '系统参数列表', '', 0, 0, 'sys/param/param-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (111, 11, 'add', '新增参数信息', '', 0, 0, 'sys/param/param-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (112, 11, 'edit', '编辑参数信息', '', 0, 0, 'sys/param/param-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (12, 1, 'dict', '字典管理', 'IconBook', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 2, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (120, 12, 'search', '字典信息列表', '', 0, 0, 'sys/dict/dict-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (121, 12, 'add', '新增字典信息', '', 0, 0, 'sys/dict/dict-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (122, 12, 'edit', '编辑字典信息', '', 0, 0, 'sys/dict/dict-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);

insert into auth_menu
values (2, 0, 'auth', '权限管理', 'IconSafe', 0, 0, 'PARENT_LAYOUT', 'Y', 'Y', 1, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (20, 2, 'user', '用户管理', 'IconUser', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 0, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (200, 20, 'search', '用户信息列表', '', 0, 0, 'auth/user/user-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (201, 20, 'add', '新增用户信息', '', 0, 0, 'auth/user/user-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp,
        1, current_timestamp);
insert into auth_menu
values (202, 20, 'edit', '编辑用户信息', '', 0, 0, 'auth/user/user-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (21, 2, 'menu', '菜单管理', 'IconApps', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 1, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (210, 21, 'search', '菜单信息列表', '', 0, 0, 'auth/menu/menu-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (211, 21, 'add', '新增菜单信息', '', 0, 0, 'auth/menu/menu-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp,
        1, current_timestamp);
insert into auth_menu
values (212, 21, 'edit', '编辑菜单信息', '', 0, 0, 'auth/menu/menu-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (22, 2, 'role', '角色管理', 'IconUserGroup', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 2, 'N', 1, null, 1,
        current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (220, 22, 'search', '角色信息列表', '', 0, 0, 'auth/role/role-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (221, 22, 'add', '新增角色信息', '', 0, 0, 'auth/role/role-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp,
        1, current_timestamp);
insert into auth_menu
values (222, 22, 'edit', '编辑角色信息', '', 0, 0, 'auth/role/role-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (23, 2, 'dept', '部门管理', '', 0, 0, 'BLANK_LAYOUT', 'Y', 'N', 3, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (230, 23, 'search', '部门信息列表', '', 0, 0, 'auth/dept/dept-search-view', 'N', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (231, 23, 'add', '新增部门信息', '', 0, 0, 'auth/dept/dept-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp,
        1, current_timestamp);
insert into auth_menu
values (232, 23, 'edit', '编辑部门信息', '', 0, 0, 'auth/dept/dept-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (3, 0, 'my-flow', '我的流程', 'IconStamp', 0, 0, 'PARENT_LAYOUT', 'Y', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (30, 3, 'todo', '我的待办', 'IconInfoCircle', 0, 0, 'my-flow/todo/todo-search-view', 'Y', 'Y', 0, 'N', 1, null,
        1,
        current_timestamp,
        1, current_timestamp);
insert into auth_menu
values (31, 3, 'done', '我的已办', 'IconCheckCircle', 0, 0, 'my-flow/done/done-search-view', 'Y', 'Y', 1, 'N', 1, null,
        1,
        current_timestamp,
        1, current_timestamp);
insert into auth_menu
values (32, 3, 'apply', '我的申请', 'IconPlusCircle', 0, 0, 'my-flow/apply/apply-search-view', 'Y', 'Y', 2, 'N', 1,
        null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (33, 3, 'care', '我的关注', 'IconExclamationCircle', 0, 0, 'my-flow/care/care-search-view', 'Y', 'Y', 3, 'N', 1,
        null, 1,
        current_timestamp,
        1, current_timestamp);

insert into auth_menu
values (4, 0, 'flow', '流程管理', 'IconMindMapping', 0, 0, 'PARENT_LAYOUT', 'Y', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (40, 4, 'model', '流程模型', 'IconCodepen', 0, 0, 'BLANK_LAYOUT', 'Y', 0, 3, 'N', 1, null, 1, current_timestamp,
        1,
        current_timestamp);
insert into auth_menu
values (400, 40, 'search', '流程模型列表', '', 0, 0, 'flow/model/model-search-view', 'N', 'Y', 0, 'N', 1, null,
        1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (401, 40, 'add', '新增流程模型', '', 0, 0, 'flow/model/model-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (402, 40, 'edit', '编辑流程模型', '', 0, 0, 'flow/model/model-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (41, 4, 'instance', '流程实例', 'IconComputer', 0, 0, 'BLANK_LAYOUT', 'Y', 0, 3, 'N', 1, null, 1,
        current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (410, 41, 'search', '流程实例列表', '', 0, 0, 'flow/instance/instance-search-view', 'N', 'Y', 0, 'N', 1, null,
        1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (411, 41, 'add', '新增流程实例', '', 0, 0, 'flow/instance/instance-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (412, 41, 'edit', '编辑流程实例', '', 0, 0, 'flow/instance/instance-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);

insert into auth_menu
values (5, 0, 'oa', 'OA管理', 'IconDesktop', 0, 0, 'PARENT_LAYOUT', 'Y', 'Y', 0, 'N', 1, null, 1,
        current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (50, 5, 'leave', '请假申请', 'IconTag', 0, 0, 'BLANK_LAYOUT', 'Y', 0, 3, 'N', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into auth_menu
values (500, 50, 'search', '请假申请列表', '', 0, 0, 'oa/leave/leave-search-view', 'N', 'Y', 0, 'N', 1, null,
        1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (501, 50, 'add', '新增请假申请', '', 0, 0, 'oa/leave/leave-edit-view', 'N', 'Y', 1, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);
insert into auth_menu
values (502, 50, 'edit', '编辑请假申请', '', 0, 0, 'oa/leave/leave-edit-view', 'N', 'Y', 2, 'N', 1, null, 1,
        current_timestamp, 1, current_timestamp);

insert into auth_action
values (1, 100, 'addNoticeAction', '添加通知公告', 1, null);
insert into auth_action
values (2, 100, 'deleteNoticeAction', '删除通知公告', 1, null);
insert into auth_action
values (3, 100, 'editNoticeAction', '修改通知公告', 1, null);
insert into auth_action
values (4, 110, 'addParamsAction', '添加系统参数', 1, null);
insert into auth_action
values (5, 110, 'deleteParamsAction', '删除系统参数', 1, null);
insert into auth_action
values (6, 110, 'editParamsAction', '修改系统参数', 1, null);
insert into auth_action
values (7, 120, 'addDictAction', '添加字典信息', 1, null);
insert into auth_action
values (8, 120, 'deleteDictAction', '删除字典信息', 1, null);
insert into auth_action
values (9, 120, 'editDictAction', '修改字典信息', 1, null);
insert into auth_action
values (10, 200, 'addUserAction', '添加用户信息', 1, null);
insert into auth_action
values (11, 200, 'deleteUserAction', '删除用户信息', 1, null);
insert into auth_action
values (12, 200, 'editUserAction', '修改用户信息', 1, null);
insert into auth_action
values (13, 210, 'addMenuAction', '添加菜单信息', 1, null);
insert into auth_action
values (14, 210, 'deleteMenuAction', '删除菜单信息', 1, null);
insert into auth_action
values (15, 210, 'editMenuAction', '修改菜单信息', 1, null);
insert into auth_action
values (16, 220, 'addRoleAction', '添加角色信息', 1, null);
insert into auth_action
values (17, 220, 'deleteRoleAction', '删除角色信息', 1, null);
insert into auth_action
values (18, 220, 'editRoleAction', '修改角色信息', 1, null);

insert into auth_perm
values (1, 1, 1, 0, 0);
insert into auth_perm
values (2, 1, 10, 0, 0);
insert into auth_perm
values (3, 1, 100, 0, 0);
insert into auth_perm
values (4, 1, 101, 0, 0);
insert into auth_perm
values (5, 1, 102, 0, 0);
insert into auth_perm
values (6, 1, 11, 0, 0);
insert into auth_perm
values (7, 1, 110, 0, 0);
insert into auth_perm
values (8, 1, 111, 0, 0);
insert into auth_perm
values (9, 1, 112, 0, 0);
insert into auth_perm
values (10, 1, 12, 0, 0);
insert into auth_perm
values (11, 1, 120, 0, 0);
insert into auth_perm
values (12, 1, 121, 0, 0);
insert into auth_perm
values (13, 1, 122, 0, 0);
-- insert into auth_perm
-- values (14, 1, 13, 0, 0);
-- insert into auth_perm
-- values (15, 1, 130, 0, 0);
-- insert into auth_perm
-- values (16, 1, 131, 0, 0);
-- insert into auth_perm
-- values (17, 1, 132, 0, 0);
insert into auth_perm
values (18, 1, 2, 0, 0);
insert into auth_perm
values (19, 1, 20, 0, 0);
insert into auth_perm
values (20, 1, 200, 0, 0);
insert into auth_perm
values (21, 1, 201, 0, 0);
insert into auth_perm
values (22, 1, 202, 0, 0);
insert into auth_perm
values (23, 1, 21, 0, 0);
insert into auth_perm
values (24, 1, 210, 0, 0);
insert into auth_perm
values (25, 1, 211, 0, 0);
insert into auth_perm
values (26, 1, 212, 0, 0);
insert into auth_perm
values (27, 1, 22, 0, 0);
insert into auth_perm
values (28, 1, 220, 0, 0);
insert into auth_perm
values (29, 1, 221, 0, 0);
insert into auth_perm
values (30, 1, 222, 0, 0);
-- insert into auth_perm
-- values (31, 1, 23, 0, 0);
-- insert into auth_perm
-- values (32, 1, 230, 0, 0);
-- insert into auth_perm
-- values (33, 1, 231, 0, 0);
-- insert into auth_perm
-- values (34, 1, 232, 0, 0);
-- insert into auth_perm
-- values (39, 1, 3, 0, 0);
-- insert into auth_perm
-- values (40, 1, 30, 0, 0);
-- insert into auth_perm
-- values (41, 1, 31, 0, 0);
-- insert into auth_perm
-- values (42, 1, 32, 0, 0);
-- insert into auth_perm
-- values (43, 1, 33, 0, 0);
-- insert into auth_perm
-- values (44, 1, 4, 0, 0);
-- insert into auth_perm
-- values (45, 1, 40, 0, 0);
-- insert into auth_perm
-- values (46, 1, 400, 0, 0);
-- insert into auth_perm
-- values (47, 1, 401, 0, 0);
-- insert into auth_perm
-- values (48, 1, 402, 0, 0);
-- insert into auth_perm
-- values (49, 1, 41, 0, 0);
-- insert into auth_perm
-- values (50, 1, 410, 0, 0);
-- insert into auth_perm
-- values (51, 1, 411, 0, 0);
-- insert into auth_perm
-- values (52, 1, 412, 0, 0);
-- insert into auth_perm
-- values (53, 1, 5, 0, 0);
-- insert into auth_perm
-- values (54, 1, 50, 0, 0);
-- insert into auth_perm
-- values (55, 1, 500, 0, 0);
-- insert into auth_perm
-- values (56, 1, 501, 0, 0);
-- insert into auth_perm
-- values (57, 1, 502, 0, 0);

insert into auth_perm
values (1000, 1, 1, 0, 1);
insert into auth_perm
values (1001, 1, 2, 0, 1);
insert into auth_perm
values (1002, 1, 3, 0, 1);

insert into sys_dict
values (1, 'sys_tag_type', '标签类型', 1, '标签类型', 1, current_timestamp,
        1, current_timestamp);
insert into sys_dict
values (2, 'sys_common_status', '系统数据通用状态', 1, '系统通用的数据状态，仅包含正常、停用状态', 1, current_timestamp,
        1, current_timestamp);
insert into sys_dict
values (3, 'sys_common_gender', '性别', 1, '性别字典', 1, current_timestamp,
        1, current_timestamp);
insert into sys_dict
values (4, 'sys_user_status', '用户状态', 1, '用户状态字典', 1, current_timestamp,
        1, current_timestamp);
insert into sys_dict
values (5, 'sys_yes_or_no', '是否', 1, '是否字典', 1, current_timestamp,
        1, current_timestamp);
insert into sys_dict
values (6, 'sys_param_type', '参数类型', 1, '参数类型字典', 1, current_timestamp,
        1, current_timestamp);
insert into sys_dict
values (7, 'sys_notice_status', '公告状态', 1, '公告状态字典', 1, current_timestamp,
        1, current_timestamp);

insert into sys_dict_item
values (1, 1, 'red', '红色', 'red', 1, null);
insert into sys_dict_item
values (2, 1, 'orangered', '橘红色', 'orangered', 1, null);
insert into sys_dict_item
values (3, 1, 'orange', '橘色', 'orange', 1, null);
insert into sys_dict_item
values (4, 1, 'gold', '金色', 'gold', 1, null);
insert into sys_dict_item
values (5, 1, 'lime', '黄绿色', 'lime', 1, null);
insert into sys_dict_item
values (6, 1, 'green', '绿色', 'green', 1, null);
insert into sys_dict_item
values (7, 1, 'cyan', '青色', 'cyan', 1, null);
insert into sys_dict_item
values (8, 1, 'blue', '蓝色', 'blue', 1, null);
insert into sys_dict_item
values (9, 1, 'arcoblue', '主题色', 'arcoblue', 1, null);
insert into sys_dict_item
values (10, 1, 'purple', '紫色', 'purple', 1, null);
insert into sys_dict_item
values (11, 1, 'pinkpurple', '粉紫色', 'pinkpurple', 1, null);
insert into sys_dict_item
values (12, 1, 'magenta', '洋红色', 'magenta', 1, null);
insert into sys_dict_item
values (13, 1, 'gray', '灰色', 'gray', 1, null);

insert into sys_dict_item
values (14, 2, '0', '停用', 'red', 1, null);
insert into sys_dict_item
values (15, 2, '1', '正常', 'green', 1, null);

insert into sys_dict_item
values (16, 3, '0', '男', 'blue', 1, null);
insert into sys_dict_item
values (17, 3, '1', '女', 'magenta', 1, null);
insert into sys_dict_item
values (18, 3, '2', '未知', 'orange', 1, null);

insert into sys_dict_item
values (19, 4, '0', '停用', 'red', 1, null);
insert into sys_dict_item
values (20, 4, '1', '正常', 'green', 1, null);
insert into sys_dict_item
values (21, 4, '2', '锁定', 'orange', 1, null);

insert into sys_dict_item
values (22, 5, 'Y', '是', 'green', 1, null);
insert into sys_dict_item
values (23, 5, 'N', '否', 'orange', 1, null);

insert into sys_dict_item
values (24, 6, 'F', '前端参数', 'green', 1, null);
insert into sys_dict_item
values (25, 6, 'B', '后端参数', 'orange', 1, null);

insert into sys_dict_item
values (26, 7, '0', '停用', 'red', 1, null);
insert into sys_dict_item
values (27, 7, '1', '正常', 'green', 1, null);
insert into sys_dict_item
values (28, 7, '2', '待推送', 'cyan', 1, null);
insert into sys_dict_item
values (29, 7, '3', '已推送', 'arcoblue', 1, null);

insert into sys_param
values (1, '新增用户默认密码', 'new_user_default_password', '1qaz!QAZ', 'B', 'Y', 1, null, 1, current_timestamp, 1,
        current_timestamp);
insert into sys_param
values (2, '是否开启验证码', 'is_open_captcha', 'true', 'F', 'Y', 1, null, 1, current_timestamp, 1,
        current_timestamp);

insert into sys_notice
values (1, 'Nuts后台管理系统有新版本啦！', 'Nuts后台管理系统有新版本啦！', 1, current_timestamp, 'Y', 1, null, 1,
        current_timestamp, 1, current_timestamp)