-- 权限管理 - 用户信息
CREATE TABLE auth_user
(
    id           bigint       NOT NULL,
    username     varchar(50)  NOT NULL,
    password     varchar(255) NOT NULL,
    nickname     varchar(50)  NOT NULL,
    gender       int,
    mobile       varchar(20),
    email        varchar(50),
    error_times  int          NOT NULL,
    locked_time  datetime,
    status       int          NOT NULL,
    remark       varchar(255),
    created_by   bigint       NOT NULL,
    created_time datetime     NOT NULL,
    updated_by   bigint       NOT NULL,
    updated_time datetime     NOT NULL,
    PRIMARY KEY (id)
);

-- 权限管理 - 用户与角色关系表
CREATE TABLE auth_user_role
(
    id  bigint NOT NULL,
    uid bigint NOT NULL,
    rid bigint NOT NULL,
    PRIMARY KEY (id)
);

-- 权限管理 - 角色信息
CREATE TABLE auth_role
(
    id           bigint      NOT NULL,
    role_code    varchar(50) NOT NULL,
    role_name    varchar(50) NOT NULL,
    status       int         NOT NULL,
    remark       varchar(255),
    created_by   bigint      NOT NULL,
    created_time datetime    NOT NULL,
    updated_by   bigint      NOT NULL,
    updated_time datetime    NOT NULL,
    PRIMARY KEY (id)
);

-- 权限管理 - 权限信息
CREATE TABLE auth_perm
(
    id           bigint  NOT NULL,
    belong_id    bigint  NOT NULL,
    subject_id   bigint  NOT NULL,
    belong_type  tinyint NOT NULL,
    subject_type tinyint NOT NULL,
    PRIMARY KEY (id)
);

-- 权限管理 - 菜单信息
CREATE TABLE auth_menu
(
    id            bigint NOT NULL,
    pid           bigint,
    menu_code     varchar(50),
    menu_name     varchar(50),
    icon          varchar(50),
    scheme        tinyint,
    target        tinyint,
    path          varchar(100),
    visible       char(1),
    show_in_bread char(1),
    sort          int,
    keep_live     char(1),
    status        int,
    remark        varchar(255),
    created_by    bigint,
    created_time  datetime,
    updated_by    bigint,
    updated_time  datetime,
    PRIMARY KEY (id)
);

-- 权限管理 - 操作信息
CREATE TABLE auth_action
(
    id          bigint NOT NULL,
    mid         bigint,
    action_code varchar(100),
    action_name varchar(100),
    status      int,
    remark      varchar(255),
    PRIMARY KEY (id)
);

-- 系统管理 - 参数信息
CREATE TABLE sys_param
(
    id           bigint NOT NULL,
    param_name   varchar(128),
    param_key    varchar(128),
    param_value  varchar(128),
    param_type   char(1),
    is_build_in  char(1),
    status       int,
    remark       varchar(255),
    created_by   bigint,
    created_time datetime,
    updated_by   bigint,
    updated_time datetime,
    PRIMARY KEY (id)
);

-- 系统管理 - 字典信息
CREATE TABLE sys_dict
(
    id           bigint NOT NULL,
    dict_name    varchar(100),
    dict_desc    varchar(255),
    status       int,
    remark       varchar(255),
    created_by   bigint,
    created_time datetime,
    updated_by   bigint,
    updated_time datetime,
    PRIMARY KEY (id)
);

-- 系统管理 - 字典项信息
CREATE TABLE sys_dict_item
(
    id              bigint NOT NULL,
    did             bigint,
    dict_item_key   varchar(128),
    dict_item_value varchar(128),
    tag_type        varchar(128),
    status          int,
    remark          varchar(255),
    PRIMARY KEY (id)
);

create table sys_notice
(
    id             bigint NOT NULL,
    notice_title   varchar(128),
    notice_content text,
    notice_by      bigint,
    notice_time    datetime,
    auto_notice    char(1),
    status         int,
    remark         varchar(255),
    created_by     bigint,
    created_time   datetime,
    updated_by     bigint,
    updated_time   datetime,
    PRIMARY KEY (id)
);

create table sys_notice_user
(
    id           bigint NOT NULL,
    nid          bigint,
    uid          bigint,
    status       int,
    created_time datetime,
    read_time    datetime,
    PRIMARY KEY (id)
);

-- 系统管理 - 登录日志
create table sys_login_log
(
    id bigint NOT NULL,
    PRIMARY KEY (id)
);

-- 系统管理 - 访问日志
create table sys_request_log
(
    id                bigint NOT NULL,
    request_ip        varchar(30),
    request_path      varchar(100),
    request_method    varchar(10),
    request_data      varchar(200),
    request_area      varchar(10),
    request_terminal  varchar(100),
    request_timestamp timestamp,
    response_code     varchar(100),
    response_time     int,
    PRIMARY KEY (id)
);

create table flow_model
(
    id           bigint NOT NULL,
    did          varchar(255),
    remark       varchar(255),
    created_by   bigint,
    created_time datetime,
    updated_by   bigint,
    updated_time datetime,
    PRIMARY KEY (id)
)
